// solver_test.go
package main

import (
	"reflect"
	"testing"
)

func TestNewPuzzle(t *testing.T) {
	rows := 3
	cols := 4
	cellValues := []string{"1 2", "3 4", " 5"}
	expectedBoard := []byte(
		"#x#x#x#x#x#\n" +
			"x.  1 2 . x\n" +
			"# 3 4   #  \n" +
			"x.     5 . x\n" +
			"#x#x#x#x#x#\n")

	puzzle := NewPuzzle(rows, cols, cellValues)
	if !reflect.DeepEqual(puzzle.Board, expectedBoard) {
		t.Errorf("NewPuzzle() failed. Expected:\n%s\nGot:\n%s", expectedBoard, string(puzzle.Board))
	}
}

func TestPuzzle_GetBoard(t *testing.T) {
	puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
	tests := []struct {
		row, col int
		expected byte
	}{
		{0, 0, '#'}, {0, 1, 'x'}, {1, 1, '.'}, {2, 2, '3'}, {3, 3, ' '}, {4, 4, '#'},
	}

	for _, test := range tests {
		got := puzzle.GetBoard(test.row, test.col)
		if got != test.expected {
			t.Errorf("GetBoard(%d, %d) = %c, expected %c", test.row, test.col, got, test.expected)
		}
	}
}

func TestPuzzle_SetBoard(t *testing.T) {
	puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
	puzzle.SetBoard(2, 3, 'x')
	expected := []byte(
		"#x#x#x#x#x#\n" +
			"x.  1 2 . x\n" +
			"# 3 4 x #  \n" +
			"x.     5 . x\n" +
			"#x#x#x#x#x#\n")

	if !reflect.DeepEqual(puzzle.Board, expected) {
		t.Errorf("SetBoard() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
	}
}

func TestPuzzle_CountAdjacentLinks(t *testing.T) {
	puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
	puzzle.SetBoard(1, 2, '|')
	puzzle.SetBoard(2, 1, '-')
	puzzle.SetBoard(2, 3, '-')
	puzzle.SetBoard(3, 2, '|')

	tests := []struct {
		row, col, expected int
	}{
		{1, 1, 0}, {2, 2, 4}, {3, 3, 0}, {2, 1, 1}, {1, 2, 1}, {3, 2, 1},
	}

	for _, test := range tests {
		got := puzzle.CountAdjacentLinks(test.row, test.col)
		if got != test.expected {
			t.Errorf("CountAdjacentLinks(%d, %d) = %d, expected %d", test.row, test.col, got, test.expected)
		}
	}
}

func TestPuzzle_CountAdjacentXes(t *testing.T) {
	puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
	puzzle.SetBoard(1, 1, 'x')
	puzzle.SetBoard(1, 3, 'x')
	puzzle.SetBoard(3, 1, 'x')
	puzzle.SetBoard(3, 3, 'x')

	tests := []struct {
		row, col, expected int
	}{
		{1, 1, 0}, {2, 2, 4}, {3, 3, 0}, {1, 2, 2}, {2, 1, 2}, {2, 3, 2},
	}

	for _, test := range tests {
		got := puzzle.CountAdjacentXes(test.row, test.col)
		if got != test.expected {
			t.Errorf("CountAdjacentXes(%d, %d) = %d, expected %d", test.row, test.col, got, test.expected)
		}
	}
}

func TestPuzzle_CondSetX(t *testing.T) {
	puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
	puzzle.CondSetX(2, 3)
	expected := []byte(
		"#x#x#x#x#x#\n" +
			"x.  1 2 . x\n" +
			"# 3 4 x #  \n" +
			"x.     5 . x\n" +
			"#x#x#x#x#x#\n")

	if !reflect.DeepEqual(puzzle.Board, expected) {
		t.Errorf("CondSetX() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
	}
}

func TestPuzzle_CondSetLink(t *testing.T) {
	puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
	puzzle.CondSetLink(2, 2, '-')
	expected := []byte(
		"#x#x#x#x#x#\n" +
			"x.  1 2 . x\n" +
			"#-3 4   #  \n" +
			"x.     5 . x\n" +
			"#x#x#x#x#x#\n")

	if !reflect.DeepEqual(puzzle.Board, expected) {
		t.Errorf("CondSetLink() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
	}
}

func TestPuzzle_IsSolved(t *testing.T) {
	puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
	puzzle.SetBoard(1, 2, '|')
	puzzle.SetBoard(2, 1, '-')
	puzzle.SetBoard(2, 3, '-')
	puzzle.SetBoard(3, 2, '|')

	if puzzle.IsSolved() {
		t.Error("IsSolved() returned true for an unsolved puzzle")
	}

	puzzle.SetBoard(1, 1, 'x')
	puzzle.SetBoard(1, 3, 'x')
	puzzle.SetBoard(2, 2, 'x')
	puzzle.SetBoard(3, 1, 'x')
	puzzle.SetBoard(3, 3, 'x')

	if !puzzle.IsSolved() {
		t.Error("IsSolved() returned false for a solved puzzle")
	}
}

func TestPuzzle_CanSolve(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    if !puzzle.CanSolve() {
        t.Error("CanSolve() returned false for a solvable puzzle")
    }

    puzzle.SetBoard(1, 1, 'x')
    puzzle.SetBoard(1, 2, 'x')
    puzzle.SetBoard(1, 3, 'x')
    puzzle.SetBoard(2, 1, 'x')
    if puzzle.CanSolve() {
        t.Error("CanSolve() returned true for an unsolvable puzzle")
    }
}

func TestPuzzle_IterCells(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    visited := make([]bool, puzzle.BoardHeight*puzzle.BoardWidth)

    puzzle.IterCells(func(p *Puzzle, row, col int) {
        visited[row*p.BoardWidth+col] = true
    })

    for r := 2; r < 2*puzzle.Rows+1; r += 2 {
        for c := 2; c < 2*puzzle.Cols+1; c += 2 {
            if !visited[r*puzzle.BoardWidth+c] {
                t.Errorf("IterCells() did not visit cell (%d, %d)", r, c)
            }
        }
    }
}

func TestPuzzle_IterDots(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    visited := make([]bool, puzzle.BoardHeight*puzzle.BoardWidth)

    puzzle.IterDots(func(p *Puzzle, row, col int) {
        visited[row*p.BoardWidth+col] = true
    })

    for r := 1; r < 2*puzzle.Rows+2; r += 2 {
        for c := 1; c < 2*puzzle.Cols+2; c += 2 {
            if !visited[r*puzzle.BoardWidth+c] {
                t.Errorf("IterDots() did not visit dot (%d, %d)", r, c)
            }
        }
    }
}

func TestPuzzle_updatePathInfo(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    dot1 := [2]int{1, 1}
    dot2 := [2]int{1, 3}
    dot3 := [2]int{3, 1}
    dot4 := [2]int{3, 3}

    puzzle.updatePathInfo(dot1, dot2)
    puzzle.updatePathInfo(dot3, dot4)

    if puzzle.DotPaths[dot1] == puzzle.DotPaths[dot3] {
        t.Error("updatePathInfo() incorrectly joined separate paths")
    }

    puzzle.updatePathInfo(dot1, dot3)

    if len(puzzle.PathDots) != 1 {
        t.Error("updatePathInfo() did not join paths correctly")
    }
}

func TestPuzzle_FillInXes(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    expected := []byte(
        "#x#x#x#x#x#\n" +
            "xXX 1 2 XX x\n" +
            "#X3 4 XXX# \n" +
            "xX     5 X x\n" +
            "#x#x#x#x#x#\n")

    puzzle.FillInXes()

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("FillInXes() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestPuzzle_FillInLinks(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"2 2", "3 3", "1 1"})
    expected := []byte(
        "#x#x#x#x#x#\n" +
            "x-+-2 2-+- x\n" +
            "#-|-3 3-|-# \n" +
            "x-|-1 1-|- x\n" +
            "#x#x#x#x#x#\n")

    puzzle.FillInLinks()

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("FillInLinks() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestPuzzle_HandleOnes(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 1", " 5"})
    expected := []byte(
        "#x#x#x#x#x#\n" +
            "xXX 1 2XX x\n" +
            "#X3XXX XXX# \n" +
            "xX     5 X x\n" +
            "#x#x#x#x#x#\n")

    puzzle.HandleOnes()

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("HandleOnes() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestPuzzle_HandleThrees(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 3", "3 2", " 5"})
    expected := []byte(
        "#x#x#x#x#x#\n" +
            "xXX 1-|-X x\n" +
            "#-|-3-|- X# \n" +
            "xX     5 X x\n" +
            "#x#x#x#x#x#\n")

    puzzle.HandleThrees()

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("HandleThrees() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestPuzzle_UpdateDotState(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    puzzle.SetBoard(1, 2, '|')
    puzzle.SetBoard(2, 1, '-')
    puzzle.SetBoard(2, 3, '-')
    puzzle.SetBoard(3, 2, '|')
    expected := []byte(
        "#x#x#x#x#x#\n" +
            "xXX 1 2XX x\n" +
            "#X3 4XXX# \n" +
            "xX     5 X x\n" +
            "#x#x#x#x#x#\n")

    puzzle.UpdateDotState()

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("UpdateDotState() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestPuzzle_AvoidMultipleLoops(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    puzzle.SetBoard(1, 2, '|')
    puzzle.SetBoard(2, 1, '-')
    puzzle.SetBoard(2, 3, '-')
    puzzle.SetBoard(3, 2, '|')
    puzzle.updatePathInfo([2]int{1, 1}, [2]int{1, 3})
    puzzle.updatePathInfo([2]int{3, 1}, [2]int{3, 3})

    expected := []byte(
        "#x#x#x#x#x#\n" +
            "xXX 1 2XX x\n" +
            "#X3 4XXX# \n" +
            "xXX    5XX x\n" +
            "#x#x#x#x#x#\n")

    puzzle.AvoidMultipleLoops()

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("AvoidMultipleLoops() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestPuzzle_HandleClosedCorners(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    puzzle.SetBoard(1, 1, 'x')
    puzzle.SetBoard(1, 3, 'x')
    puzzle.SetBoard(3, 1, 'x')
    puzzle.SetBoard(3, 3, 'x')
    expected := []byte(
        "#x#x#x#x#x#\n" +
            "xXX 1 2XX x\n" +
            "#X3-|-4XX# \n" +
            "xXX    5XX x\n" +
            "#x#x#x#x#x#\n")

    puzzle.HandleClosedCorners()

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("HandleClosedCorners() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestPuzzle_HandleDiagonalChains(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 3", "2 2", " 5"})
    expected := []byte(
        "#x#x#x#x#x#\n" +
            "xXX 1-|-X x\n" +
            "#-|-2-|- X# \n" +
            "xX    -5 X x\n" +
            "#x#x#x#x#x#\n")

    puzzle.HandleDiagonalChains()

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("HandleDiagonalChains() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestPuzzle_CheckRowLinks(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    puzzle.SetBoard(2, 1, '-')
    puzzle.SetBoard(2, 3, '-')
    puzzle.SetBoard(2, 5, '|')
    expected := []byte(
        "#x#x#x#x#x#\n" +
            "x.  1 2 . x\n" +
            "#-3 4-|- X# \n" +
            "x.     5 . x\n" +
            "#x#x#x#x#x#\n")

    puzzle.CheckRowLinks()

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("CheckRowLinks() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestPuzzle_CheckColLinks(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    puzzle.SetBoard(1, 2, '|')
    puzzle.SetBoard(3, 2, '|')
    puzzle.SetBoard(5, 2, ' ')
    expected := []byte(
        "#x#x#x#x#x#\n" +
            "x.-1 2X. x\n" +
            "#X3 4 X  # \n" +
            "x.-    5X. x\n" +
            "#x#x#x#x#x#\n")

    puzzle.CheckColLinks()

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("CheckColLinks() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestPuzzle_ScoreMove(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    tests := []struct {
        row, col, expected int
    }{
        {2, 2, 0}, {2, 4, 5}, {4, 2, -10}, {4, 4, 0},
    }

    for _, test := range tests {
        score := puzzle.ScoreMove(test.row, test.col)
        if score != test.expected {
            t.Errorf("ScoreMove(%d, %d) = %d, expected %d", test.row, test.col, score, test.expected)
        }
    }
}

func TestPuzzle_EnumerateMoves(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    puzzle.SetBoard(1, 2, '|')
    puzzle.SetBoard(2, 1, '-')
    puzzle.SetBoard(2, 3, '-')
    puzzle.SetBoard(3, 2, '|')

    moves := puzzle.EnumerateMoves()
    expected := [][3]int{
        {1, 1, 5}, {1, 3, 5}, {3, 1, 5}, {3, 3, 5},
    }

    if !reflect.DeepEqual(moves, expected) {
        t.Errorf("EnumerateMoves() failed. Expected:\n%v\nGot:\n%v", expected, moves)
    }
}

func TestPuzzle_ApplyMove(t *testing.T) {
    puzzle := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    puzzle.ApplyMove([3]int{1, 1, '-'})
    expected := []byte(
        "#x#x#x#x#x#\n" +
            "x-. 1 2 . x\n" +
            "# 3 4   #  \n" +
            "x.     5 . x\n" +
            "#x#x#x#x#x#\n")

    if !reflect.DeepEqual(puzzle.Board, expected) {
        t.Errorf("ApplyMove() failed. Expected:\n%s\nGot:\n%s", expected, string(puzzle.Board))
    }
}

func TestLoadPuzzle(t *testing.T) {
    puzzle, err := loadPuzzle("testdata/puzzle.txt")
    if err != nil {
        t.Errorf("loadPuzzle() failed: %v", err)
    }

    expected := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    if !reflect.DeepEqual(puzzle, expected) {
        t.Errorf("loadPuzzle() failed. Expected:\n%v\nGot:\n%v", expected, puzzle)
    }
}

func TestSolvePuzzle(t *testing.T) {
    // Start a test server
    go func() {
        app := fiber.New()
        app.Post("/solve", solvePuzzleAPI)
        app.Listen(":8000")
    }()

    // Load a test puzzle
    puzzle, err := loadPuzzle("testdata/puzzle.txt")
    if err != nil {
        t.Errorf("loadPuzzle() failed: %v", err)
    }

    // Solve the puzzle
    solved := SolvePuzzle(puzzle)
    if !solved {
        t.Error("SolvePuzzle() failed to solve the puzzle")
    }

    // Check the solution
    expected := NewPuzzle(3, 4, []string{"1 2", "3 4", " 5"})
    expected.SetBoard(1, 1, 'x')
    expected.SetBoard(1, 3, 'x')
    expected.SetBoard(2, 2, 'x')
    expected.SetBoard(3, 1, 'x')
    expected.SetBoard(3, 3, 'x')

    if !reflect.DeepEqual(puzzle, expected) {
        t.Errorf("SolvePuzzle() returned an incorrect solution. Expected:\n%v\nGot:\n%v", expected, puzzle)
    }
}

func TestGetPuzzle(t *testing.T) {
    // Start a test server
    go func() {
        app := fiber.New()
        app.Get("/generate", generatePuzzleAPI)
        app.Listen(":8000")
    }()

    // Get a new puzzle
    puzzle := GetPuzzle("http://www.puzzle-loop.com/?v=0&size=5")

    // Check the puzzle format
    lines := strings.Split(puzzle, "\n")
    if len(lines) != 6 {
        t.Errorf("GetPuzzle() returned an invalid puzzle format: %s", puzzle)
    }

    dimensions := strings.Split(lines[0], " ")
    if len(dimensions) != 2 {
        t.Errorf("GetPuzzle() returned an invalid puzzle dimensions: %s", lines[0])
    }

    rows, err := strconv.Atoi(dimensions[0])
    if err != nil {
        t.Errorf("GetPuzzle() returned an invalid puzzle rows: %s", dimensions[0])
    }

    cols, err := strconv.Atoi(dimensions[1])
    if err != nil {
        t.Errorf("GetPuzzle() returned an invalid puzzle cols: %s", dimensions[1])
    }

    if rows < 3 || rows > 25 || cols < 3 || cols > 30 {
        t.Errorf("GetPuzzle() returned an invalid puzzle size: %dx%d", rows, cols)
    }
}