package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

const databaseURL = "file:slitherlink.db?cache=shared&mode=memory"

type SlitherLink struct {
	ID       int
	Puzzle   string
	Solution string
}

func openDB() (*sql.DB, error) {
	db, err := sql.Open("sqlite3", databaseURL)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS data (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			puzzle TEXT NOT NULL,
			solution TEXT
		)
	`)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func getSlitherLink(db *sql.DB, id int) (*SlitherLink, error) {
	row := db.QueryRow("SELECT id, puzzle, solution FROM data WHERE id = ?", id)
	var sl SlitherLink
	err := row.Scan(&sl.ID, &sl.Puzzle, &sl.Solution)
	if err != nil {
		return nil, err
	}
	return &sl, nil
}

func getAllSlitherLinks(db *sql.DB) ([]*SlitherLink, error) {
	rows, err := db.Query("SELECT id, puzzle, solution FROM data")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var slitherLinks []*SlitherLink
	for rows.Next() {
		var sl SlitherLink
		err = rows.Scan(&sl.ID, &sl.Puzzle, &sl.Solution)
		if err != nil {
			return nil, err
		}
		slitherLinks = append(slitherLinks, &sl)
	}

	return slitherLinks, nil
}

func insertSlitherLink(db *sql.DB, puzzle, solution string) (int64, error) {
	result, err := db.Exec("INSERT INTO data (puzzle, solution) VALUES (?, ?)", puzzle, solution)
	if err != nil {
		return 0, err
	}
	return result.LastInsertId()
}

func updateSlitherLink(db *sql.DB, id int, puzzle, solution string) error {
	_, err := db.Exec("UPDATE data SET puzzle = ?, solution = ? WHERE id = ?", puzzle, solution, id)
	return err
}

func deleteSlitherLink(db *sql.DB, id int) error {
	_, err := db.Exec("DELETE FROM data WHERE id = ?", id)
	return err
}

func main() {
	db, err := openDB()
	if err != nil {
		fmt.Println("Error opening database:", err)
		return
	}
	defer db.Close()

	// Приклади використання функцій
	id, err := insertSlitherLink(db, "puzzle", "solution")
	if err != nil {
		fmt.Println("Error inserting SlitherLink:", err)
	} else {
		fmt.Println("Inserted SlitherLink with ID:", id)
	}

	sl, err := getSlitherLink(db, int(id))
	if err != nil {
		fmt.Println("Error getting SlitherLink:", err)
	} else {
		fmt.Println("Got SlitherLink:", sl)
	}

	err = updateSlitherLink(db, int(id), "updated puzzle", "updated solution")
	if err != nil {
		fmt.Println("Error updating SlitherLink:", err)
	}

	err = deleteSlitherLink(db, int(id))
	if err != nil {
		fmt.Println("Error deleting SlitherLink:", err)
	}

	slitherLinks, err := getAllSlitherLinks(db)
	if err != nil {
		fmt.Println("Error getting all SlitherLinks:", err)
	} else {
		fmt.Println("All SlitherLinks:", slitherLinks)
	}
}