# Web Framework
gofiber/fiber == 2.42.0  # https://github.com/gofiber/fiber

github.com/gofiber/fiber/v2/middleware/logger

# HTML Parsing
golang.org/x/net == 0.5.0  # https://pkg.go.dev/golang.org/x/net