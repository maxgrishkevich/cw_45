package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type Payload struct {
	Rows       int    `json:"rows"`
	Cols       int    `json:"cols"`
	CellValues string `json:"cell_values"`
}

type Response struct {
	Solution string `json:"solution"`
	Detail   string `json:"detail"`
}

func loadPuzzle(filename string) (int, int, string, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return 0, 0, "", err
	}

	lines := strings.Split(string(data), "\n")
	dims := strings.Split(lines[0], " ")
	rows, cols := parseIntOrZero(dims[0]), parseIntOrZero(dims[1])
	cellValues := strings.Join(lines[1:], "\n")

	return rows, cols, cellValues, nil
}

func parseIntOrZero(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}

func solveAPI(puzzleFile string) {
	rows, cols, cellValues, err := loadPuzzle(puzzleFile)
	if err != nil {
		fmt.Printf("Error loading puzzle: %v\n", err)
		return
	}

	url := "http://localhost:8000/solve"
	payload := Payload{Rows: rows, Cols: cols, CellValues: cellValues}
	jsonPayload, _ := json.Marshal(payload)

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonPayload))
	if err != nil {
		fmt.Printf("Error making request: %v\n", err)
		return
	}
	defer resp.Body.Close()

	var response Response
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		fmt.Printf("Error decoding response: %v\n", err)
		return
	}

	if resp.StatusCode == 200 {
		if response.Solution != "" {
			fmt.Println("Solution:")
			fmt.Println(response.Solution)
		} else {
			fmt.Println("Error: Could not solve the puzzle")
		}
	} else {
		fmt.Printf("Error: %s\n", response.Detail)
	}
}

func generateAPI() {
	url := "http://localhost:8000/generate"
	params := "url=http://www.puzzle-loop.com/?v=0&size=10"
	resp, err := http.Get(url + "?" + params)
	if err != nil {
		fmt.Printf("Error making request: %v\n", err)
		return
	}
	defer resp.Body.Close()

	var response Response
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		fmt.Printf("Error decoding response: %v\n", err)
		return
	}

	if resp.StatusCode == 200 {
		if response.Solution != "" {
			fmt.Println(response.Solution)
		} else {
			fmt.Println("Error: Cannot generate a puzzle")
		}
	} else {
		fmt.Printf("Error: %s\n", response.Detail)
	}
}

func getData(dataID int) {
	url := fmt.Sprintf("http://localhost:8000/data/%d", dataID)
	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("Error making request: %v\n", err)
		return
	}
	defer resp.Body.Close()

	var data Payload
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		fmt.Printf("Error decoding response: %v\n", err)
		return
	}

	if resp.StatusCode == 200 {
		fmt.Printf("Rows: %d, Cols: %d\n", data.Rows, data.Cols)
		fmt.Println("Cell values:")
		fmt.Println(data.CellValues)
	} else {
		var response Response
		err = json.NewDecoder(resp.Body).Decode(&response)
		if err != nil {
			fmt.Printf("Error decoding response: %v\n", err)
			return
		}
		fmt.Printf("Error: %s\n", response.Detail)
	}
}

func main() {
	// solveAPI("puzzle_file.txt")
	generateAPI()
	// getData(1)
}