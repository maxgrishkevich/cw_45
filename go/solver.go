import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"time"
)

const (
	OutsideColor = 'o'
)

type MoveError struct {
	value string
}

func (e *MoveError) Error() string {
	return e.value
}

func cellFuncFillInXes(puzzle *Puzzle, row, col int) {
	cellVal := puzzle.GetBoard(row, col)
	if cellVal != ' ' {
		linksRequired := int(cellVal - '0')

		numLinks := puzzle.CountAdjacentLinks(row, col)
		numXes := puzzle.CountAdjacentXes(row, col)

		if linksRequired == numLinks && (numLinks+numXes) != 4 {
			puzzle.CondSetX(row-1, col)
			puzzle.CondSetX(row+1, col)
			puzzle.CondSetX(row, col-1)
			puzzle.CondSetX(row, col+1)
		}
	}
}

func dotFuncFillInXesLinks(puzzle *Puzzle, row, col int) {
	numLinks := puzzle.CountAdjacentLinks(row, col)
	numXes := puzzle.CountAdjacentXes(row, col)

	if numLinks == 2 && numXes < 2 {
		puzzle.CondSetX(row-1, col)
		puzzle.CondSetX(row+1, col)
		puzzle.CondSetX(row, col-1)
		puzzle.CondSetX(row, col+1)
	} else if numXes == 2 && numLinks == 1 {
		puzzle.CondSetLink(row-1, col, '|')
		puzzle.CondSetLink(row+1, col, '|')
		puzzle.CondSetLink(row, col-1, '-')
		puzzle.CondSetLink(row, col+1, '-')
	} else if numXes == 3 {
		puzzle.CondSetX(row-1, col)
		puzzle.CondSetX(row+1, col)
		puzzle.CondSetX(row, col-1)
		puzzle.CondSetX(row, col+1)
	}
}

func cellFuncFillInLinks(puzzle *Puzzle, row, col int) {
	cellVal := puzzle.GetBoard(row, col)
	if cellVal != ' ' {
		linksRequired := int(cellVal - '0')

		numLinks := puzzle.CountAdjacentLinks(row, col)
		numXes := puzzle.CountAdjacentXes(row, col)

		if numLinks < linksRequired && (4-numXes) == linksRequired {
			puzzle.CondSetLink(row-1, col, '-')
			puzzle.CondSetLink(row+1, col, '-')
			puzzle.CondSetLink(row, col-1, '|')
			puzzle.CondSetLink(row, col+1, '|')
		}
	}
}

func cellFuncHandleAdjacentThrees(puzzle *Puzzle, row, col int) {
	if puzzle.GetBoard(row, col) != '3' {
		return
	}

	prevRow := row - 2
	nextRow := row + 2
	nextCol := col + 2

	if nextRow < puzzle.BoardHeight && puzzle.GetBoard(nextRow, col) == '3' {
		puzzle.CondSetLink(row-1, col, '-')
		puzzle.CondSetLink(row+1, col, '-')
		puzzle.CondSetLink(row+3, col, '-')

		puzzle.CondSetX(row+1, col-2)
		puzzle.CondSetX(row+1, col+2)
	} else if nextCol < puzzle.BoardWidth && puzzle.GetBoard(row, nextCol) == '3' {
		puzzle.CondSetLink(row, col-1, '|')
		puzzle.CondSetLink(row, col+1, '|')
		puzzle.CondSetLink(row, col+3, '|')

		puzzle.CondSetX(row-2, col+1)
		puzzle.CondSetX(row+2, col+1)
	} else if nextRow < puzzle.BoardHeight && nextCol < puzzle.BoardWidth && puzzle.GetBoard(nextRow, nextCol) == '3' {
		puzzle.CondSetLink(row-1, col, '-')
		puzzle.CondSetLink(row, col-1, '|')

		puzzle.CondSetLink(row+2, col+3, '|')
		puzzle.CondSetLink(row+3, col+2, '-')
	} else if prevRow >= 0 && nextCol < puzzle.BoardWidth && puzzle.GetBoard(prevRow, nextCol) == '3' {
		puzzle.CondSetLink(row+1, col, '-')
		puzzle.CondSetLink(row, col-1, '|')

		puzzle.CondSetLink(row-2, col+3, '|')
		puzzle.CondSetLink(row-3, col+2, '-')
	}
}

func cellFuncHandleDiagonalOnes(puzzle *Puzzle, row, col int) {
	if puzzle.GetBoard(row, col) != '1' {
		return
	}

	for dr := -1; dr <= 1; dr += 2 {
		for dc := -1; dc <= 1; dc += 2 {
			nextRow := row + 2*dr
			nextCol := col + 2*dc

			if nextRow < 0 || nextRow >= puzzle.BoardHeight || nextCol < 0 || nextCol >= puzzle.BoardWidth {
				continue
			}

			if puzzle.GetBoard(nextRow, nextCol) != '1' {
				continue
			}

			if puzzle.GetBoard(row, col-dc) == 'x' && puzzle.GetBoard(row-dr, col) == 'x' {
				puzzle.CondSetX(nextRow, nextCol+dc)
				puzzle.CondSetX(nextRow+dr, nextCol)
			} else if puzzle.GetBoard(row, col+dc) == 'x' && puzzle.GetBoard(row+dr, col) == 'x' {
				puzzle.CondSetX(nextRow, nextCol-dc)
				puzzle.CondSetX(nextRow-dr, nextCol)
			}
		}
	}
}

func cellFuncHandleDiagonalChains(puzzle *Puzzle, row, col int) {
	if puzzle.GetBoard(row, col) != '3' {
		return
	}

	for dr := -1; dr <= 1; dr += 2 {
		for dc := -1; dc <= 1; dc += 2 {
			i := 0
			for {
				i++
				nextRow := row + 2*dr*i
				nextCol := col + 2*dc*i

				if nextRow < 0 || nextRow >= puzzle.BoardHeight || nextCol < 0 || nextCol >= puzzle.BoardWidth {
					break
				}

				if puzzle.GetBoard(nextRow, nextCol) == '3' {
					puzzle.CondSetLink(row, col-dc, '|')
					puzzle.CondSetLink(row-dr, col, '-')

					puzzle.CondSetLink(nextRow, nextCol+dc, '|')
					puzzle.CondSetLink(nextRow+dr, nextCol, '-')
				} else if puzzle.GetBoard(nextRow, nextCol) == '2' {
					side1 := puzzle.GetBoard(nextRow, nextCol+dc)
					side2 := puzzle.GetBoard(nextRow+dr, nextCol)
					if (side1 == '-' || side1 == '|') && side2 == 'x' || (side2 == '-' || side2 == '|') && side1 == 'x' {
						puzzle.CondSetLink(row, col-dc, '|')
                        puzzle.CondSetLink(row-dr, col, '-')
					}
				} else {
					break
				}
			}
		}
	}
}

func cellFuncHandleLinksThrees(puzzle *Puzzle, row, col int) {
	cellVal := puzzle.GetBoard(row, col)
	if cellVal != '3' {
		return
	}

	numLinks := puzzle.CountAdjacentLinks(row, col)
	if numLinks >= 2 {
		return
	}

	for dr := -1; dr <= 1; dr += 2 {
		for dc := -1; dc <= 1; dc += 2 {
			dotRow := row + dr
			dotCol := col + dc

			if puzzle.GetBoard(dotRow+dr, dotCol) == 'x' && puzzle.GetBoard(dotRow, dotCol+dc) == 'x' &&
				puzzle.GetBoard(dotRow-dr, dotCol) != 'x' && puzzle.GetBoard(dotRow, dotCol-dc) != 'x' {
				puzzle.CondSetLink(row, col+dc, '|')
				puzzle.CondSetLink(row+dr, col, '-')
			}

			if (puzzle.GetBoard(dotRow+dr, dotCol) == '-' || puzzle.GetBoard(dotRow+dr, dotCol) == '|' ||
				puzzle.GetBoard(dotRow, dotCol+dc) == '-' || puzzle.GetBoard(dotRow, dotCol+dc) == '|') &&
				puzzle.GetBoard(row-dr, col) != 'x' && puzzle.GetBoard(row, col-dc) != 'x' {
				puzzle.CondSetLink(row-dr, col, '-')
				puzzle.CondSetLink(row, col-dc, '|')
			}
		}
	}
}

func dotFuncAvoidMultipleLoops(puzzle *Puzzle, row, col int) {
	if len(puzzle.PathDots) == 1 {
		return
	}

	nextRow := row + 2
	nextCol := col + 2

	dot1 := [2]int{row, col}
	if _, ok := puzzle.DotPaths[dot1]; !ok {
		return
	}

	if nextRow < puzzle.BoardHeight {
		dot2 := [2]int{nextRow, col}
		if path2, ok := puzzle.DotPaths[dot2]; ok && puzzle.DotPaths[dot1] == path2 {
			puzzle.CondSetX(row+1, col)
		}
	}

	if nextCol < puzzle.BoardWidth {
		dot2 := [2]int{row, nextCol}
		if path2, ok := puzzle.DotPaths[dot2]; ok && puzzle.DotPaths[dot1] == path2 {
			puzzle.CondSetX(row, col+1)
		}
	}
}

func cellFuncHandleClosedCorners(puzzle *Puzzle, row, col int) {
	cellVal := puzzle.GetBoard(row, col)
	if cellVal == ' ' {
		return
	}

	for dr := -1; dr <= 1; dr += 2 {
		for dc := -1; dc <= 1; dc += 2 {
			corner := puzzle.GetBoard(row+2*dr, col+dc) == 'x' && puzzle.GetBoard(row+dr, col+2*dc) == 'x'

			if corner {
				if cellVal == '1' {
					puzzle.CondSetX(row+dr, col)
					puzzle.CondSetX(row, col+dc)
				} else if cellVal == '2' && puzzle.GetBoard(row+2*dr, col-dc) == 'x' && puzzle.GetBoard(row-dr, col+2*dc) == 'x' {
					puzzle.CondSetLink(row+dr, col-2*dc, '-')
					puzzle.CondSetLink(row-2*dr, col+dc, '|')
				} else if cellVal == '3' {
					puzzle.CondSetLink(row+dr, col, '-')
					puzzle.CondSetLink(row, col+dc, '|')
				}
			}
		}
	}
}

type Puzzle struct {
	Rows         int
	Cols         int
	BoardWidth   int
	BoardHeight  int
	Board        []byte
	NextPathID   int
	DotPaths     map[[2]int]int
	PathDots     map[int][][2]int
	Changed      bool
	ChangeCount  int
	BoardColors  []byte
}

func NewPuzzle(rows, cols int, cellValues []string) *Puzzle {
	puzzle := &Puzzle{
		Rows:       rows,
		Cols:       cols,
		BoardWidth: 2*(cols+1) + 1,
		BoardHeight: 2*(rows+1) + 1,
		Board:      make([]byte, 0, puzzle.BoardHeight*puzzle.BoardWidth),
		NextPathID: 1,
		DotPaths:   make(map[[2]int]int),
		PathDots:   make(map[int][][2]int),
		BoardColors: make([]byte, (rows+2)*(cols+2)),
	}

	topBottomRow := bytes.Repeat([]byte("#x"), cols+1)
	topBottomRow = append(topBottomRow, '#')
	dotRow := []byte("x. ")
	dotRow = append(dotRow, bytes.Repeat([]byte(". "), cols)...)
	dotRow = append(dotRow, ".x"...)

	puzzle.Board = append(puzzle.Board, topBottomRow...)
	for r := 0; r < rows; r++ {
		puzzle.Board = append(puzzle.Board, dotRow...)

		rowValues := []byte{'#', ' '}
		for _, ch := range cellValues[r] {
			rowValues = append(rowValues, byte(ch), ' ')
		}
		rowValues = append(rowValues, '#')
		puzzle.Board = append(puzzle.Board, rowValues...)
	}
	puzzle.Board = append(puzzle.Board, dotRow...)
	puzzle.Board = append(puzzle.Board, topBottomRow...)

	for r := 0; r < rows+2; r++ {
		for c := 0; c < cols+2; c++ {
			if r == 0 || r == rows+1 || c == 0 || c == cols+1 {
				puzzle.BoardColors[r*(cols+2)+c] = OutsideColor
			} else {
				puzzle.BoardColors[r*(cols+2)+c] = 'i'
			}
		}
	}

	return puzzle
}

func (p *Puzzle) GetBoard(row, col int) byte {
	return p.Board[row*p.BoardWidth+col]
}

func (p *Puzzle) SetBoard(row, col int, val byte) {
	p.Board[row*p.BoardWidth+col] = val
}

func (p *Puzzle) SetBoardColor(row, col int, val byte) {
	p.BoardColors[row*(p.Cols+2)+col] = val
}

func (p *Puzzle) GetBoardColor(row, col int) byte {
	return p.BoardColors[row*(p.Cols+2)+col]
}

func (p *Puzzle) IsChanged() bool {
	return p.Changed
}

func (p *Puzzle) SetChanged(value ...bool) {
	if len(value) > 0 {
		p.Changed = value[0]
	} else {
		p.Changed = true
	}
}

func (p *Puzzle) ClearChangedCount() {
	p.Changed = false
	p.ChangeCount = 0
}

func (p *Puzzle) GetBoardAsString() string {
	return string(p.Board)
}

func (p *Puzzle) PrettyPrint(includeXes, includeNumbers bool) {
	fmt.Printf("Puzzle size: %d x %d\n", p.Rows, p.Cols)
	for r := 0; r < p.BoardHeight; r++ {
		if r%2 == 0 {
			for c := 0; c < p.BoardWidth; c += 2 {
				if c%2 == 0 {
					if includeNumbers {
						fmt.Print(string(p.Board[r*p.BoardWidth+c]))
					} else {
						fmt.Print(" ")
					}
				} else {
					if includeNumbers {
						fmt.Print(string(p.Board[r*p.BoardWidth+c]))
					}
				}
			}
			fmt.Println()
		} else {
			for c := 0; c < p.BoardWidth; c += 2 {
				if includeNumbers {
					fmt.Print(string(p.Board[r*p.BoardWidth+c]))
				} else {
					fmt.Print(" ")
				}
			}
			fmt.Println()
		}
	}
	fmt.Println()
	return
}

func (p *Puzzle) PrettyPrint() {
	p.PrettyPrint(true, true)
}

func main() {}

func NewPuzzle() *Puzzle {
	return &Puzzle{}
}

type Puzzle struct {
	Rows       int
	Cols       int
	BoardWidth int
	BoardHeight int
	Board      []byte
}

func (p *Puzzle) SetBoard() {
	p.Board = make([]byte, p.BoardWidth*p.BoardHeight+1)
	copy(p.Board, bytes.Repeat([]byte("$#$"), p.BoardWidth*p.BoardHeight+1))
	for i := 0; i < len(p.Board); i += 2 {
		p.Board[i] = '$'
	}
}

func SolutionFor(boardWidth, boardHeight int) []byte {
	return make([]byte, boardWidth*boardHeight)
}
func BoardDimensions(dims ...int) []int {
	lenDims := len(dims)
	if lenDims == 0 {
		return []int{}
	} else if lenDims == 1 {
		return []int{dims[0]}
	} else if lenDims%2 == 0 {
		return []int{int(math.Pow(2, 3))}
	} else {
		return []int{
			14, 32, 7, 31, 13,
			27, 9, 1, 26, 17,
			3, 18, 17, 13, 28,
			3, 17, 28, 17, 9,
			32, 13, 28, 17, 9,
			32, 17, 28, 17, 9,
			3, 32, 17, 28, 17,
			32, 13, 28, 17, 9,
		}
	}
}

func (p *Puzzle) PrettyPrint() {
	p.PrettyPrint(true, true)
}

func (p *Puzzle) IsSolved() bool {
	// Має бути лише один шлях
	if len(p.PathDots) > 1 {
		return false
	}

	// Кожна клітинка з числом повинна мати відповідну кількість ліній навколо неї
	for r := 2; r < 2*p.Rows+1; r += 2 {
		for c := 2; c < 2*p.Cols+1; c += 2 {
			val := p.GetBoard(r, c)
			if val != ' ' {
				required := int(val - '0')
				actual := p.CountAdjacentLinks(r, c)
				if required != actual {
					return false
				}
			}
		}
	}

	// Кожна точка на шляху повинна мати рівно дві лінії навколо неї (це забезпечує замкнутий шлях)
	pathID := 0
	for id := range p.PathDots {
		pathID = id
		break
	}
	path := p.PathDots[pathID]
	for _, dot := range path {
		r, c := dot[0], dot[1]
		links := p.CountAdjacentLinks(r, c)
		if links != 2 {
			return false
		}
	}

	return true
}

func (p *Puzzle) CanSolve() bool {
	for r := 2; r < 2*p.Rows+1; r += 2 {
		for c := 2; c < 2*p.Cols+1; c += 2 {
			val := p.GetBoard(r, c)
			if val != ' ' {
				required := int(val - '0')
				actual := p.CountAdjacentLinks(r, c)
				xes := p.CountAdjacentXes(r, c)

				if 4-xes < required {
					fmt.Println("Can't solve: too many x-es around the cell")
					return false
				}

				if actual+xes == 4 && required != actual {
					fmt.Println("Can't solve: wrong number of links around cell")
					return false
				}
			}
		}
	}

	return true
}

func (p *Puzzle) IterCells(cellFunc func(*Puzzle, int, int)) {
	for r := 2; r < 2*p.Rows+1; r += 2 {
		for c := 2; c < 2*p.Cols+1; c += 2 {
			cellFunc(p, r, c)
		}
	}
}

func (p *Puzzle) IterDots(dotFunc func(*Puzzle, int, int)) {
	for r := 1; r < 2*p.Rows+2; r += 2 {
		for c := 1; c < 2*p.Cols+2; c += 2 {
			dotFunc(p, r, c)
		}
	}
}

func (p *Puzzle) CountAdjacentLinks(row, col int) int {
	count := 0
	for r := row - 1; r <= row+1; r++ {
		for c := col - 1; c <= col+1; c++ {
			if p.GetBoard(r, c) == '-' || p.GetBoard(r, c) == '|' {
				count++
			}
		}
	}
	return count
}

func (p *Puzzle) CountAdjacentXes(row, col int) int {
	count := 0
	for r := row - 1; r <= row+1; r++ {
		for c := col - 1; c <= col+1; c++ {
			if p.GetBoard(r, c) == 'x' {
				count++
			}
		}
	}
	return count
}

func (p *Puzzle) CondSetX(row, col int) {
	if p.GetBoard(row, col) == ' ' {
		p.SetBoard(row, col, 'x')
		p.SetChanged()
		p.ChangeCount++
	}
}

func (p *Puzzle) CondSetLink(row, col int, value byte) {
	if value != '-' && value != '|' {
		return
	}

	if p.GetBoard(row, col) == ' ' {
		if value == '-' {
			if p.GetBoard(row, col-1) != '.' || p.GetBoard(row, col+1) != '.' {
				return
			}
		} else {
			if p.GetBoard(row-1, col) != '.' || p.GetBoard(row+1, col) != '.' {
				return
			}
		}

		p.SetBoard(row, col, value)
		p.SetChanged()
		p.ChangeCount++

		if value == '-' {
			dot1 := [2]int{row, col - 1}
			dot2 := [2]int{row, col + 1}
			p.updatePathInfo(dot1, dot2)
		} else {
			dot1 := [2]int{row - 1, col}
			dot2 := [2]int{row + 1, col}
			p.updatePathInfo(dot1, dot2)
		}
	}
}

func (p *Puzzle) updatePathInfo(dot1, dot2 [2]int) {
	if _, ok := p.DotPaths[dot1]; !ok && _, ok := p.DotPaths[dot2]; !ok {
		pathID := p.NextPathID
		p.NextPathID++
		p.DotPaths[dot1] = pathID
		p.DotPaths[dot2] = pathID
		p.PathDots[pathID] = [][2]int{dot1, dot2}
	} else if _, ok := p.DotPaths[dot1]; !ok {
		pathID := p.DotPaths[dot2]
		p.DotPaths[dot1] = pathID
		p.PathDots[pathID] = append(p.PathDots[pathID], dot1)
	} else if _, ok := p.DotPaths[dot2]; !ok {
		pathID := p.DotPaths[dot1]
		p.DotPaths[dot2] = pathID
		p.PathDots[pathID] = append(p.PathDots[pathID], dot2)
	} else {
		dot1PathID := p.DotPaths[dot1]
		dot2PathID := p.DotPaths[dot2]

		if dot1PathID == dot2PathID {
			if len(p.PathDots) > 1 {
				panic(&MoveError{value: fmt.Sprintf("Can't join dots %v and %v", dot1, dot2)})
			}
			return
		}

		dot2PathDots := p.PathDots[dot2PathID]
		for _, dot := range dot2PathDots {
			p.DotPaths[dot] = dot1PathID
		}

		p.PathDots[dot1PathID] = append(p.PathDots[dot1PathID], dot2PathDots...)
		delete(p.PathDots, dot2PathID)
	}
}

func (p *Puzzle) FillInXes() {
	p.IterCells(cellFuncFillInXes)
}

func (p *Puzzle) FillInLinks() {
	p.IterCells(cellFuncFillInLinks)
}

func (p *Puzzle) HandleOnes() {
	p.IterCells(cellFuncHandleDiagonalOnes)
}

func (p *Puzzle) HandleThrees() {
	p.IterCells(cellFuncHandleAdjacentThrees)
	p.IterCells(cellFuncHandleLinksThrees)
}

func (p *Puzzle) UpdateDotState() {
	p.IterDots(dotFuncFillInXesLinks)
}

func (p *Puzzle) AvoidMultipleLoops() {
	p.IterDots(dotFuncAvoidMultipleLoops)
}

func (p *Puzzle) HandleClosedCorners() {
	p.IterCells(cellFuncHandleClosedCorners)
}

func (p *Puzzle) HandleDiagonalChains() {
	p.IterCells(cellFuncHandleDiagonalChains)
}

func (p *Puzzle) CheckRowLinks() {
	for r := 2; r < 2*p.Rows+1; r += 2 {
		numLinks := 0
		numUnknowns := 0
		var unknown [2]int

		for c := 1; c < 2*p.Cols+2; c += 2 {
			val := p.GetBoard(r, c)
			if val == '|' {
				numLinks++
			} else if val == ' ' {
				numUnknowns++
				if numUnknowns == 1 {
					unknown = [2]int{r, c}
				}
			}
		}

		if numUnknowns == 1 {
			r, c := unknown[0], unknown[1]
			if numLinks%2 == 0 {
				p.CondSetX(r, c)
			} else {
				p.CondSetLink(r, c, '|')
			}
		}
	}
}

func (p *Puzzle) CheckColLinks() {
	for c := 2; c < 2*p.Cols+1; c += 2 {
		numLinks := 0
		numUnknowns := 0
		var unknown [2]int

		for r := 1; r < 2*p.Rows+2; r += 2 {
			val := p.GetBoard(r, c)
			if val == '-' {
				numLinks++
			} else if val == ' ' {
				numUnknowns++
				if numUnknowns == 1 {
					unknown = [2]int{r, c}
				}
			}
		}

		if numUnknowns == 1 {
			r, c := unknown[0], unknown[1]
			if numLinks%2 == 0 {
				p.CondSetX(r, c)
			} else {
				p.CondSetLink(r, c, '-')
			}
		}
	}
}

func (p *Puzzle) IterSolve(verbose bool) {
	operations := []struct {
		op   func()
		name string
	}{
		{p.HandleClosedCorners, "Handling closed corners"},
		{p.FillInXes, "Filling in x-es based on cell values"},
		{p.FillInLinks, "Filling in links based on cell values"},
		{p.HandleThrees, "Filling in links based on adjacent 3-cells"},
		{p.HandleOnes, "Filling in x-es based on adjacent 1-cells"},
		{p.UpdateDotState, "Filling in x-es and links based on state adjacent to dots"},
		{p.AvoidMultipleLoops, "Avoid multiple loops"},
	}

	iter := 0
	for {
		iter++
		madeChange := false

		for _, op := range operations {
			p.SetChanged(false)
			op.op()
			if p.IsChanged() {
				if verbose {
					fmt.Printf("%d: %s\n", iter, op.name)
				}
				madeChange = true
			}
		}

		if !madeChange {
			p.SetChanged(false)
			p.HandleDiagonalChains()
			if p.IsChanged() {
				if verbose {
					fmt.Printf("%d: (ADV) Handle diagonal chains\n", iter)
				}
				madeChange = true
			}

			p.SetChanged(false)
			p.CheckRowLinks()
			p.CheckColLinks()
			if p.IsChanged() {
				if verbose {
					fmt.Printf("%d: (ADV) Check row/column links\n", iter)
				}
				madeChange = true
			}
		}

		if !p.CanSolve() {
			fmt.Println("Cannot solve this board: invalid configuration reached.")
			break
		}

		if !madeChange {
			break
		}
	}
}

func (p *Puzzle) DotsAreConnected(dot1, dot2 [2]int) bool {
	if _, ok := p.DotPaths[dot1]; ok && _, ok := p.DotPaths[dot2]; ok {
		dot1PathID := p.DotPaths[dot1]
		dot2PathID := p.DotPaths[dot2]
		return dot1PathID == dot2PathID
	}
	return false
}

func (p *Puzzle) ScoreMove(cellR, cellC int) int {
	score := 0
	cellVal := p.GetBoard(cellR, cellC)
	if cellVal >= '0' && cellVal <= '3' {
		cellVal -= '0'
		links := p.CountAdjacentLinks(cellR, cellC)

		if int(cellVal) == links {
			score = -10
		} else {
			score = 5 - int(cellVal) + links
		}
	}
	return score
}

func (p *Puzzle) EnumerateMoves() [][3]int {
	var moves [][3]int
	for r := 1; r < 2*p.Rows+2; r += 2 {
		for c := 1; c < 2*p.Cols+2; c += 2 {
			if p.CountAdjacentLinks(r, c) == 1 {
				dot := [2]int{r, c}
				if p.GetBoard(r, c-1) == ' ' && !p.DotsAreConnected(dot, [2]int{r, c - 1}) {
					score := p.ScoreMove(r-1, c-1) + p.ScoreMove(r+1, c-1)
					moves = append(moves, [3]int{r, c - 1, score})
				}
				if p.GetBoard(r, c+1) == ' ' && !p.DotsAreConnected(dot, [2]int{r, c + 1}) {
					score := p.ScoreMove(r-1, c+1) + p.ScoreMove(r+1, c+1)
					moves = append(moves, [3]int{r, c + 1, score})
				}
				if p.GetBoard(r-1, c) == ' ' && !p.DotsAreConnected(dot, [2]int{r - 1, c}) {
					score := p.ScoreMove(r-1, c-1) + p.ScoreMove(r-1, c+1)
					moves = append(moves, [3]int{r - 1, c, score})
				}
				if p.GetBoard(r+1, c) == ' ' && !p.DotsAreConnected(dot, [2]int{r + 1, c}) {
					score := p.ScoreMove(r+1, c-1) + p.ScoreMove(r+1, c+1)
					moves = append(moves, [3]int{r + 1, c, score})
				}
			}
		}
	}

	sort.Slice(moves, func(i, j int) bool {
		return moves[i][2] > moves[j][2]
	})

	return moves
}

func (p *Puzzle) ApplyMove(move [3]int) {
	p.CondSetLink(move[0], move[1], byte(move[2]))
}

func loadPuzzle(filename string) (*Puzzle, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	line := scanner.Text()
	dims := strings.Split(line, " ")
	rows, err := strconv.Atoi(dims[0])
	if err != nil {
		return nil, err
	}
	cols, err := strconv.Atoi(dims[1])
	if err != nil {
		return nil, err
	}

	cellValues := make([]string, rows)
	for i := 0; i < rows; i++ {
		scanner.Scan()
		cellValues[i] = scanner.Text()
	}

	return NewPuzzle(rows, cols, cellValues), nil
}

func solvePuzzle(p *Puzzle) bool {
	attempts := [][5]interface{}{
		{p, 1, nil, 0, 0},
	}
	skipped := 0

	if !p.CanSolve() {
		fmt.Println("solve-puzzle was handed a board it couldn't solve!")
		return false
	}

	for len(attempts) > 0 {
		fmt.Printf("%d more board configurations to try.\n", len(attempts))

		info := attempts[rand.Intn(len(attempts))]
		attempts = attempts[:len(attempts)-1]

		p, depth, move, iMove, nMoves := info[0].(*Puzzle), info[1].(int), info[2], info[3].(int), info[4].(int)

		if depth > 1 {
			fmt.Printf("DEPTH %d: Move %d of %d: %v Attempting to solve.\n", depth, iMove, nMoves, move)
		} else {
			fmt.Println("Attempting initial solution.")
		}

		try {
			p.IterSolve()
		} catch {
			fmt.Println("Encountered an invalid move. Abandoning this path.")
			continue
		}

		if p.IsSolved() {
			fmt.Printf("DEPTH %d: SOLVED\n", depth)
			p.PrettyPrint()
			return true
		} else if !p.CanSolve() {
			fmt.Println("Couldn't solve this configuration, abandoning.")
			continue
		} else {
			fmt.Printf("DEPTH %d: NOT SOLVED (change-count = %d)\n", depth, p.ChangeCount)

			moves := p.EnumerateMoves()
			fmt.Printf("From this board configuration, found %d more moves.\n", len(moves))

			totalMoves := len(moves)
			newMoves := 0
			moveInfos := make([][5]interface{}, 0, totalMoves)
			for i := range moves {
				move := moves[i]

				pCopy := *p
				pCopy.ClearChangedCount()
				pCopy.ApplyMove(move)

				boardStr := pCopy.GetBoardAsString()
				if _, ok := boardSet[boardStr]; ok {
					skipped++
					continue
				}

				newMoves++
				moveInfos = append(moveInfos, [5]interface{}{&pCopy, depth + 1, move, i + 1, totalMoves})
			}

			if len(moveInfos) > 10 {
				rand.Shuffle(len(moveInfos), func(i, j int) {
					moveInfos[i], moveInfos[j] = moveInfos[j], moveInfos[i]
				})
				moveInfos = moveInfos[:10]
				newMoves = len(moveInfos)
			}

			attempts = append(attempts, moveInfos...)
			fmt.Printf("Added %d new moves to the set of attempts.\n", newMoves)
		}
	}

	fmt.Println("Couldn't solve puzzle.")
	return false
}
