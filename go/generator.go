package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	_ "github.com/go-sql-driver/mysql"
)

type SlitherLink struct {
	Puzzle string
}

func getPuzzle(pageURL string) (string, error) {
	res, err := http.Get(pageURL)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return "", err
	}

	puzzleTable := doc.Find("table#LoopTable")
	puzzleRows := puzzleTable.Find("tr:nth-child(even)") // Выбираем каждую вторую строку
	rowSpecs := make([]string, 0)

	puzzleRows.Each(func(_ int, row *goquery.Selection) {
		rowSpec := ""
		row.Find("td:nth-child(even)").Each(func(_ int, cell *goquery.Selection) { // Выбираем каждую вторую ячейку
			cellVal := strings.TrimSpace(cell.Text())
			if cellVal == "" {
				cellVal = " "
			}
			rowSpec += cellVal
		})
		rowSpecs = append(rowSpecs, rowSpec)
	})

	output := fmt.Sprintf("%d %d\n", len(rowSpecs), len(rowSpecs[0]))
	output += strings.Join(rowSpecs, "\n")

	timestamp := time.Now().Format("20060102150405")
	outputFilename := fmt.Sprintf("puzzles/%s.txt", timestamp)

	err = writeToFile(outputFilename, output)
	if err != nil {
		return "", err
	}

	return output, nil
}

func writeToFile(filename, content string) error {
	return nil // Реализацию функции оставляю на ваше усмотрение
}

func savePuzzleConditions(conditions string, db *sql.DB) error {
	_, err := db.Exec("INSERT INTO SlitherLink (puzzle) VALUES (?)", conditions)
	return err
}

func generateAndSavePuzzleConditions(url string) error {
	db, err := sql.Open("mysql", "user:password@/database")
	if err != nil {
		return err
	}
	defer db.Close()

	puzzleConditions, err := getPuzzle(url)
	if err != nil {
		return err
	}

	err = savePuzzleConditions(puzzleConditions, db)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	// Пример использования
	err := generateAndSavePuzzleConditions("http://www.puzzle-loop.com/?v=0&size=5")
	if err != nil {
		fmt.Println("Ошибка:", err)
	}
}