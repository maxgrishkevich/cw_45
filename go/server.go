package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	_ "github.com/mattn/go-sqlite3"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type PuzzleData struct {
	Rows       int    `json:"rows"`
	Cols       int    `json:"cols"`
	CellValues string `json:"cell_values"`
}

type SlitherLink struct {
	ID       int    `json:"id"`
	Puzzle   string `json:"puzzle"`
	Solution string `json:"solution"`
}

var db *sql.DB

func initDB() {
	var err error
	db, err = sql.Open("sqlite3", "slitherlink.db")
	if err != nil {
		log.Fatal(err)
	}
}

func getPuzzleData(c echo.Context) error {
	id := c.Param("id")

	var puzzle SlitherLink
	err := db.QueryRow("SELECT * FROM data WHERE id = ?", id).Scan(&puzzle.ID, &puzzle.Puzzle, &puzzle.Solution)
	if err != nil {
		return c.JSON(http.StatusNotFound, map[string]string{"error": "Puzzle not found"})
	}

	return c.JSON(http.StatusOK, puzzle)
}

func solvePuzzle(c echo.Context) error {
	puzzleData := new(PuzzleData)
	if err := c.Bind(puzzleData); err != nil {
		return err
	}

	// Call the solver function with the puzzle data
	solution, err := solvePuzzleHelper(puzzleData.Rows, puzzleData.Cols, strings.Split(puzzleData.CellValues, "\n"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]string{"solution": solution})
}

func generatePuzzle(c echo.Context) error {
	url := c.QueryParam("url")
	if url == "" {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "Missing URL parameter"})
	}

	puzzle, err := getPuzzleFromURL(url)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, map[string]string{"response": puzzle})
}

func main() {
	initDB()
	defer db.Close()

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.POST("/solve", solvePuzzle)
	e.GET("/generate", generatePuzzle)
	e.GET("/data/:id", getPuzzleData)

	e.Logger.Fatal(e.Start(":8000"))
}