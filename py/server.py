from pydantic import BaseModel
from sqlalchemy.orm import Session
from fastapi import HTTPException, status, FastAPI, Depends
from fastapi.params import Query
from fastapi.security import HTTPBearer
from solver import Puzzle, solve_puzzle
import generator
from database import Base, engine, get_db, SlitherLink

app = FastAPI()
security = HTTPBearer()
ACCESS_TOKEN = "token"


class HTTPError(BaseModel):
    detail: str


class PuzzleData(BaseModel):
    rows: int
    cols: int
    cell_values: str


async def get_current_token(token: str = Depends(security)):
    if token != ACCESS_TOKEN:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect token",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return token


@app.get("/data/{data_id}")
async def get_data(data_id: int, db: Session = Depends(get_db), token: str = Depends(get_current_token)):
    data = db.query(SlitherLink).filter(data_id == SlitherLink.id).first()
    if data:
        return {
            "puzzle": data.puzzle,
            "solution": data.solution
        }
    else:
        raise HTTPException(status_code=404, detail="Condition not found")


@app.post("/solve")
async def solve_puzzle_api(puzzle_data: PuzzleData, token: str = Depends(get_current_token)):
    try:
        p = Puzzle(puzzle_data.rows, puzzle_data.cols, puzzle_data.cell_values.split(sep='\n'))
        if p.can_solve():
            solved = solve_puzzle(p)
            if solved:
                return {"solution": p.pretty_print()}
            else:
                return {"error": "Could not solve the puzzle"}
        else:
            raise HTTPException(status_code=400, detail="Invalid puzzle configuration")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.get("/generate")
async def generate_puzzle_api(url: str = Query(...), token: str = Depends(get_current_token)):
    try:
        new_puzzle = generator.get_puzzle(url)
        return {"response": new_puzzle}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


Base.metadata.create_all(bind=engine)