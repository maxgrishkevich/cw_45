import datetime
import requests
from bs4 import BeautifulSoup
from sqlalchemy.orm import Session
from database import SessionLocal
from database import SlitherLink


def get_puzzle(page_url):
    page = requests.get(page_url)
    soup = BeautifulSoup(page.text, 'html.parser')

    puzzle_table = soup.find('table', id='LoopTable')

    puzzle_rows = puzzle_table.findAll('tr')
    puzzle_rows = puzzle_rows[1::2]

    row_specs = []

    for row in puzzle_rows:
        puzzle_cols = row.findAll('td')
        puzzle_cols = puzzle_cols[1::2]

        row_spec = ''

        for col in puzzle_cols:
            cellval = col.string
            if not cellval:
                cellval = ' '
            row_spec += cellval

        row_specs.append(row_spec)

    output = "%d %d\n" % (len(row_specs), len(row_specs[0]))
    output += '\n'.join(row_specs)

    print(output)

    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    output_filename = f"puzzles/{timestamp}.txt"

    with open(output_filename, 'w') as file:
        file.write(output)

    return output


def save_puzzle_conditions(conditions, db: Session):
    condition = SlitherLink(puzzle=conditions)
    db.add(condition)
    db.commit()


def generate_and_save_puzzle_conditions(url):
    db = SessionLocal()
    puzzle_conditions = get_puzzle(url)
    save_puzzle_conditions(puzzle_conditions, db)
    db.close()
