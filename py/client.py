import requests
import sys
from sqlalchemy import true, false
import server


def load_puzzle(filename):
    f = open(filename)
    line = f.readline()
    dims = line.split()
    rows = int(dims[0])
    cols = int(dims[1])
    cell_values = f.read()
    f.close()
    return rows, cols, cell_values


def solve_api(path):
    rows, cols, cell_values = load_puzzle(path)

    url = "http://localhost:8000/solve"
    payload = {"rows": rows, "cols": cols, "cell_values": cell_values, "token":'token'}
    response = requests.post(url, json=payload)

    if response.status_code == 200:
        solution = response.json().get("solution")
        if solution:
            print("Solution:")
            print(solution)
        else:
            print("Error: Could not solve the puzzle")
    else:
        error = response.json().get("detail")
        print(f"Error: {error}")


def generate_api(param):
    url = "http://localhost:8000/generate"
    params = {"url": f'http://www.puzzle-loop.com/?v=0&size={param}'}
    response = requests.get(url, params=params)

    if response.status_code == 200:
        puzzle = response.json().get("response")
        if puzzle:
            print(puzzle)
        else:
            print("Error: Cannot generate a puzzle")
    else:
        error = response.json().get("detail")
        print(f"Error: {error}")


def get_data(data_id):
    url = f"http://localhost:8000/data/{data_id}"
    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()
        print(f"Rows: {data['rows']}, Cols: {data['cols']}")
        print("Cell values:")
        for row in data['cell_values']:
            print(row)
    else:
        print(f"Error: {response.json()['detail']}")


if __name__ == "__main__":
    print('+------------------------------------------------+')
    print('| Hi there!                                      |')
    print('| First of all, give me your access token        |')
    print('+------------------------------------------------+')
    token = input()
    if token == 'token':
        go = true
        while go:
            print('+------------------------------------------------+')
            print('| Choose your option                             |')
            print('| 1 - solve puzzle                               |')
            print('| 2 - generate puzzle                            |')
            print('| 3 - get puzzle by id                           |')
            print('| 0 - exit                                       |')
            print('+------------------------------------------------+')
            option = input()
            if option == '1':
                print('+------------------------------------------------+')
                print('| Great! Give me path to your file               |')
                print('+------------------------------------------------+')
                path = input()
                solve_api(path)
            elif option == '2':
                print('+------------------------------------------------+')
                print('| Ok! Choose parameter:                          |')
                print('| [no size] = 5x5 normal                         |')
                print('| 1 = 10x10 normal                               |')
                print('| 2 = 15x15 normal                               |')
                print('| 3 = 20x20 normal                               |')
                print('| 4 = 5x5 hard                                   |')
                print('| 5 = 10x10 hard                                 |')
                print('| 6 = 15x15 hard                                 |')
                print('| 7 = 20x20 hard                                 |')
                print('| 8 = 25x30 normal                               |')
                print('| 9 = 25x30 hard                                 |')
                print('| 10 = 7x7 normal                                |')
                print('| 11 = 7x7 hard                                  |')
                print('| 12 = special weekly loop                       |')
                print('| 13 = special daily loop                        |')
                print('| 14 = special monthly loop                      |')
                print('+------------------------------------------------+')
                param = input()
                generate_api(param)
            elif option == '3':
                print('+------------------------------------------------+')
                print('| Fine! Give me id                               |')
                print('+------------------------------------------------+')
                ident = input()
                get_data(ident)
            elif option == '0':
                print('+------------------------------------------------+')
                print('| See you! Bye!                                  |')
                print('+------------------------------------------------+')
                go = false
            else:
                go = false
    else:
        print('Access denied')
        exit(1)
