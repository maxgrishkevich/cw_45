from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker, declarative_base

SQLALCHEMY_DATABASE_URL = "mysql://root:root@host/slitherlink"

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


class SlitherLink(Base):
    __tablename__ = 'data'

    id = Column(Integer, primary_key=True)
    puzzle = Column(String)
    solution = Column(String, nullable=True)

    def __repr__(self):
        return f"Data(id={self.id}, puzzle={self.puzzle}, solution='{self.solution}')"


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
