import copy
import random
import multiprocessing


class MoveError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def cell_func_fill_in_xes(puzzle, row, col):
    cell_val = puzzle.get_board(row, col)
    if cell_val != ' ':
        links_required = int(cell_val)

        num_links = puzzle.count_adjacent_links(row, col)
        num_xes = puzzle.count_adjacent_xes(row, col)

        if links_required == num_links and (num_links + num_xes) != 4:
            puzzle.cond_set_x(row - 1, col)
            puzzle.cond_set_x(row + 1, col)
            puzzle.cond_set_x(row, col - 1)
            puzzle.cond_set_x(row, col + 1)


def dot_func_fill_in_xes_links(puzzle, row, col):
    num_links = puzzle.count_adjacent_links(row, col)
    num_xes = puzzle.count_adjacent_xes(row, col)

    if num_links == 2 and num_xes < 2:
        puzzle.cond_set_x(row - 1, col)
        puzzle.cond_set_x(row + 1, col)
        puzzle.cond_set_x(row, col - 1)
        puzzle.cond_set_x(row, col + 1)

    elif num_xes == 2 and num_links == 1:
        puzzle.cond_set_link(row - 1, col, '|')
        puzzle.cond_set_link(row + 1, col, '|')
        puzzle.cond_set_link(row, col - 1, '-')
        puzzle.cond_set_link(row, col + 1, '-')

    elif num_xes == 3:
        puzzle.cond_set_x(row - 1, col)
        puzzle.cond_set_x(row + 1, col)
        puzzle.cond_set_x(row, col - 1)
        puzzle.cond_set_x(row, col + 1)


def cell_func_fill_in_links(puzzle, row, col):
    cell_val = puzzle.get_board(row, col)
    if cell_val != ' ':
        links_required = int(cell_val)
        num_links = puzzle.count_adjacent_links(row, col)
        num_xes = puzzle.count_adjacent_xes(row, col)

        if num_links < links_required and (4 - num_xes) == links_required:
            puzzle.cond_set_link(row - 1, col, '-')
            puzzle.cond_set_link(row + 1, col, '-')
            puzzle.cond_set_link(row, col - 1, '|')
            puzzle.cond_set_link(row, col + 1, '|')


def cell_func_handle_adjacent_threes(puzzle, row, col):
    if puzzle.get_board(row, col) != '3':
        return

    prev_row = row - 2
    next_row = row + 2
    next_col = col + 2

    if next_row < puzzle.board_height and puzzle.get_board(next_row, col) == '3':
        puzzle.cond_set_link(row - 1, col, '-')
        puzzle.cond_set_link(row + 1, col, '-')
        puzzle.cond_set_link(row + 3, col, '-')
        puzzle.cond_set_x(row + 1, col - 2)
        puzzle.cond_set_x(row + 1, col + 2)

    elif next_col < puzzle.board_width and puzzle.get_board(row, next_col) == '3':
        puzzle.cond_set_link(row, col - 1, '|')
        puzzle.cond_set_link(row, col + 1, '|')
        puzzle.cond_set_link(row, col + 3, '|')
        puzzle.cond_set_x(row - 2, col + 1)
        puzzle.cond_set_x(row + 2, col + 1)

    elif (next_row < puzzle.board_height and next_col < puzzle.board_width
          and puzzle.get_board(next_row, next_col) == '3'):
        puzzle.cond_set_link(row - 1, col, '-')
        puzzle.cond_set_link(row, col - 1, '|')

        puzzle.cond_set_link(row + 2, col + 3, '|')
        puzzle.cond_set_link(row + 3, col + 2, '-')

    elif prev_row >= 0 and next_col < puzzle.board_width and puzzle.get_board(prev_row, next_col) == '3':
        puzzle.cond_set_link(row + 1, col, '-')
        puzzle.cond_set_link(row, col - 1, '|')
        puzzle.cond_set_link(row - 2, col + 3, '|')
        puzzle.cond_set_link(row - 3, col + 2, '-')


def cell_func_handle_diagonal_ones(puzzle, row, col):
    if puzzle.get_board(row, col) != '1':
        return

    for dr in [-1, 1]:
        for dc in [-1, 1]:
            next_row = row + 2 * dr
            next_col = col + 2 * dc

            if next_row < 0 or next_row >= puzzle.board_height or \
                    next_col < 0 or next_col >= puzzle.board_width:
                continue

            if puzzle.get_board(next_row, next_col) != '1':
                continue

            if puzzle.get_board(row, col - dc) == 'x' and \
                    puzzle.get_board(row - dr, col) == 'x':
                puzzle.cond_set_x(next_row, next_col + dc)
                puzzle.cond_set_x(next_row + dr, next_col)

            elif puzzle.get_board(row, col + dc) == 'x' and puzzle.get_board(row + dr, col) == 'x':
                puzzle.cond_set_x(next_row, next_col - dc)
                puzzle.cond_set_x(next_row - dr, next_col)


def cell_func_handle_diagonal_chains(puzzle, row, col):

    if puzzle.get_board(row, col) != '3':
        return

    for dr in [-1, 1]:
        for dc in [-1, 1]:
            i = 0
            while True:
                i += 1
                next_row = row + 2 * dr * i
                next_col = col + 2 * dc * i

                if next_row < 0 or next_row >= puzzle.board_height or \
                        next_col < 0 or next_col >= puzzle.board_width:
                    break

                if puzzle.get_board(next_row, next_col) == '3':
                    puzzle.cond_set_link(row, col - dc, '|')
                    puzzle.cond_set_link(row - dr, col, '-')
                    puzzle.cond_set_link(next_row, next_col + dc, '|')
                    puzzle.cond_set_link(next_row + dr, next_col, '-')

                elif puzzle.get_board(next_row, next_col) == '2':
                    side1 = puzzle.get_board(next_row, next_col + dc)
                    side2 = puzzle.get_board(next_row + dr, next_col)
                    if (side1 in ['-', '|'] and side2 == 'x') or \
                            (side2 in ['-', '|'] and side1 == 'x'):
                        puzzle.cond_set_link(row, col - dc, '|')
                        puzzle.cond_set_link(row - dr, col, '-')
                else:
                    break


def cell_func_handle_links_threes(puzzle, row, col):
    cell_val = puzzle.get_board(row, col)
    if cell_val != '3':
        return
    num_links = puzzle.count_adjacent_links(row, col)
    if num_links >= 2:
        return

    for dr in [-1, 1]:
        for dc in [-1, 1]:
            dot_row = row + dr
            dot_col = col + dc

            if puzzle.get_board(dot_row + dr, dot_col) == 'x' and \
                    puzzle.get_board(dot_row, dot_col + dc) == 'x' and \
                    puzzle.get_board(dot_row - dr, dot_col) != 'x' and \
                    puzzle.get_board(dot_row, dot_col - dc) != 'x':
                puzzle.cond_set_link(row, col + dc, '|')
                puzzle.cond_set_link(row + dr, col, '-')

            if ((puzzle.get_board(dot_row + dr, dot_col) in ['-', '|'] or
                 puzzle.get_board(dot_row, dot_col + dc) in ['-', '|']) and puzzle.get_board(row - dr, col) != 'x'
                    and puzzle.get_board(row, col - dc) != 'x'):
                puzzle.cond_set_link(row - dr, col, '-')
                puzzle.cond_set_link(row, col - dc, '|')


def dot_func_avoid_multiple_loops(puzzle, row, col):

    if len(puzzle.path_dots) == 1:
        return

    next_row = row + 2
    next_col = col + 2

    dot1 = (row, col)
    if dot1 not in puzzle.dot_paths:
        return

    if next_row < puzzle.board_height:
        dot2 = (next_row, col)
        if dot2 in puzzle.dot_paths and \
                puzzle.dot_paths[dot1] == puzzle.dot_paths[dot2]:
            puzzle.cond_set_x(row + 1, col)

    if next_col < puzzle.board_width:
        dot2 = (row, next_col)
        if dot2 in puzzle.dot_paths and \
                puzzle.dot_paths[dot1] == puzzle.dot_paths[dot2]:
            puzzle.cond_set_x(row, col + 1)


def cell_func_handle_closed_corners(puzzle, row, col):
    cell_val = puzzle.get_board(row, col)
    if cell_val == ' ':
        return

    for dr in [-1, 1]:
        for dc in [-1, 1]:
            corner = puzzle.get_board(row + 2 * dr, col + dc) == 'x' and \
                     puzzle.get_board(row + dr, col + 2 * dc) == 'x'

            if corner:
                if cell_val == '1':
                    puzzle.cond_set_x(row + dr, col)
                    puzzle.cond_set_x(row, col + dc)

                elif (cell_val == '2' and puzzle.get_board(row + 2 * dr, col - dc) == 'x'
                      and puzzle.get_board(row - dr, col + 2 * dc) == 'x'):
                    puzzle.cond_set_link(row + dr, col - 2 * dc, '-')
                    puzzle.cond_set_link(row - 2 * dr, col + dc, '|')

                elif cell_val == '3':
                    puzzle.cond_set_link(row + dr, col, '-')
                    puzzle.cond_set_link(row, col + dc, '|')


OUTSIDE_COLOR = 'o'


class Puzzle:
    def __init__(self, rows, cols, cell_values):
        self.rows = rows
        self.cols = cols
        self.board_width = 2 * (cols + 1) + 1
        self.board_height = 2 * (rows + 1) + 1
        self.board = bytearray()
        top_bottom_row = b'#x' * (cols + 1) + b'#'
        dot_row = b'x' + b'. ' * cols + b'.x'

        assert len(top_bottom_row) == self.board_width, \
            'board-width %d does not match length of top/bottom row %d:\n"%s"' % \
            (self.board_width, len(top_bottom_row), top_bottom_row)

        assert len(dot_row) == self.board_width, \
            'board-width %d does not match length of dot-row %d:\n"%s"' % \
            (self.board_width, len(dot_row), dot_row)

        self.board.extend(top_bottom_row)

        for r in range(rows):
            self.board.extend(dot_row)
            row_values = [bytes(ch, 'ascii') for ch in cell_values[r]]
            cell_row = b'# ' + b' '.join(row_values) + b' #'
            assert len(cell_row) == self.board_width, \
                'board-width %d does not match length of cell-row %d:\n"%s"' % \
                (self.board_width, len(cell_row), cell_row)
            self.board.extend(cell_row)

        self.board.extend(dot_row)
        self.board.extend(top_bottom_row)

        assert float(len(self.board)) / self.board_width == float(self.board_height), \
            'board-height %d does not match length of board %d' % \
            (len(self.board), self.board_height)

        self.next_path_id = 1
        self.dot_paths = {}
        self.path_dots = {}
        self.changed = False
        self.change_count = 0
        self.board_colors = bytearray((self.rows + 2) * (self.cols + 2))

        for r in range(1, self.rows + 2):
            for c in range(1, self.cols + 2):
                self.set_board_color(r, c, 'i')

        for r in range(self.rows + 2):
            self.set_board_color(r, 0, OUTSIDE_COLOR)
            self.set_board_color(r, self.cols + 1, OUTSIDE_COLOR)

        for c in range(self.cols + 2):
            self.set_board_color(0, c, OUTSIDE_COLOR)
            self.set_board_color(self.rows + 1, c, OUTSIDE_COLOR)

    def get_board(self, r, c):
        assert 0 <= r < self.board_height, \
            "Invalid row index %d (must be in range [0, %d)" % \
            (r, self.board_height)
        assert 0 <= c < self.board_width, \
            "Invalid column index %d (must be in range [0, %d)" % \
            (c, self.board_width)

        return chr(self.board[r * self.board_width + c])

    def set_board(self, r, c, val):
        assert 0 <= r < self.board_height, \
            "Invalid row index %d (must be in range [0, %d)" % \
            (r, self.board_height)
        assert 0 <= c < self.board_width, \
            "Invalid column index %d (must be in range [0, %d)" % \
            (c, self.board_width)
        self.board[r * self.board_width + c] = ord(val)

    def set_board_color(self, r, c, val):
        assert 0 <= r < self.rows + 2
        assert 0 <= c < self.cols + 2
        self.board_colors[r * (self.cols + 2) + c] = ord(val)

    def get_board_color(self, r, c):
        assert 0 <= r < self.rows + 2
        assert 0 <= c < self.cols + 2
        return chr(self.board_colors[r * (self.cols + 2) + c])

    def is_changed(self):
        return self.changed

    def set_changed(self, value=True):
        self.changed = value

    def clear_changed_count(self):
        self.changed = False
        self.change_count = 0

    def get_board_as_string(self):
        return str(self.board)

    def pretty_print(self, include_xes=True, include_numbers=True):
        response = ''
        response += "Puzzle size:  %d x %d" % (self.rows, self.cols) + '\n'
        print("Puzzle size:  %d x %d" % (self.rows, self.cols))

        for r in range(1, 2 * self.rows + 2):
            row_start = r * self.board_width
            row_end = row_start + self.board_width
            row_data = self.board[row_start:row_end]
            row_data = row_data.decode('ascii')

            if not include_numbers:
                for i in range(len(row_data)):
                    if row_data[i] in ['0', '1', '2', '3']:
                        row_data[i] = ' '

            if not include_xes:
                for i in range(len(row_data)):
                    if row_data[i] == 'x':
                        row_data[i] = ' '
            response += ' '.join(row_data[1:2 * self.cols + 2]) + '\n'
            print(' '.join(row_data[1:2 * self.cols + 2]))

        return response

    def is_solved(self):
        if len(self.path_dots) > 1:
            return False

        for r in range(2, 2 * self.rows + 1, 2):
            for c in range(2, 2 * self.cols + 1, 2):
                val = self.get_board(r, c)
                if val != ' ':
                    required = int(val)
                    actual = self.count_adjacent_links(r, c)
                    if required != actual:
                        return False

        path_id = list(self.path_dots.keys())[0]
        path = self.path_dots[path_id]
        for dot in path:
            (r, c) = dot
            links = self.count_adjacent_links(r, c)
            assert links <= 2, \
                "Invalid number of links at (%d,%d):  %d" % (r, c, links)
            if links != 2:
                return False

        return True

    def can_solve(self):
        for r in range(2, 2 * self.rows + 1, 2):
            for c in range(2, 2 * self.cols + 1, 2):
                val = self.get_board(r, c)
                if val != ' ':
                    required = int(val)
                    actual = self.count_adjacent_links(r, c)
                    xes = self.count_adjacent_xes(r, c)

                    if 4 - xes < required:
                        print("Can't solve:  too many x-es around the cell")
                        return False

                    if actual + xes == 4 and required != actual:
                        print("Can't solve:  wrong number of links around cell")
                        return False

        return True

    def iter_cells(self, cell_func):
        for r in range(2, 2 * self.rows + 1, 2):
            for c in range(2, 2 * self.cols + 1, 2):
                cell_func(self, r, c)

    def iter_dots(self, dot_func):
        for r in range(1, 2 * self.rows + 2, 2):
            for c in range(1, 2 * self.cols + 2, 2):
                dot_func(self, r, c)

    def count_adjacent_links(self, row, col):
        count = 0
        for r in range(row - 1, row + 2):
            for c in range(col - 1, col + 2):
                if self.get_board(r, c) in ['-', '|']:
                    count += 1
        return count

    def count_adjacent_xes(self, row, col):
        count = 0
        for r in range(row - 1, row + 2):
            for c in range(col - 1, col + 2):
                if self.get_board(r, c) == 'x':
                    count += 1
        return count

    def cond_set_x(self, row, col):
        if self.get_board(row, col) == ' ':
            self.set_board(row, col, 'x')
            self.set_changed()
            self.change_count += 1

    def cond_set_link(self, row, col, value):
        assert (value in ['-', '|'])

        if self.get_board(row, col) == ' ':
            if value == '-':
                assert (self.get_board(row, col - 1) == '.' and
                        self.get_board(row, col + 1) == '.')
            else:
                assert (self.get_board(row - 1, col) == '.' and
                        self.get_board(row + 1, col) == '.')

            self.set_board(row, col, value)
            self.set_changed()
            self.change_count += 1

            if value == '-':
                dot1 = (row, col - 1)
                dot2 = (row, col + 1)
            else:
                dot1 = (row - 1, col)
                dot2 = (row + 1, col)

            if dot1 not in self.dot_paths and dot2 not in self.dot_paths:
                path_id = self.next_path_id
                self.next_path_id += 1
                self.dot_paths[dot1] = path_id
                self.dot_paths[dot2] = path_id
                self.path_dots[path_id] = [dot1, dot2]

            elif dot1 not in self.dot_paths:
                path_id = self.dot_paths[dot2]
                self.dot_paths[dot1] = path_id
                self.path_dots[path_id].append(dot1)

            elif dot2 not in self.dot_paths:
                path_id = self.dot_paths[dot1]
                self.dot_paths[dot2] = path_id
                self.path_dots[path_id].append(dot2)

            else:
                dot1_path_id = self.dot_paths[dot1]
                dot2_path_id = self.dot_paths[dot2]

                if dot1_path_id == dot2_path_id:
                    if len(self.path_dots) > 1:
                        raise MoveError("Can't join dots %s and %s" % (dot1, dot2))
                    else:
                        return

                dot2_path_dots = self.path_dots[dot2_path_id]
                for dot in dot2_path_dots:
                    self.dot_paths[dot] = dot1_path_id

                self.path_dots[dot1_path_id].extend(dot2_path_dots)
                del self.path_dots[dot2_path_id]

    def fill_in_xes(self):
        self.iter_cells(cell_func_fill_in_xes)

    def fill_in_links(self):
        self.iter_cells(cell_func_fill_in_links)

    def handle_ones(self):
        self.iter_cells(cell_func_handle_diagonal_ones)

    def handle_threes(self):
        self.iter_cells(cell_func_handle_adjacent_threes)
        self.iter_cells(cell_func_handle_links_threes)

    def update_dot_state(self):
        self.iter_dots(dot_func_fill_in_xes_links)

    def avoid_multiple_loops(self):
        self.iter_dots(dot_func_avoid_multiple_loops)

    def handle_closed_corners(self):
        self.iter_cells(cell_func_handle_closed_corners)

    def handle_diagonal_chains(self):
        self.iter_cells(cell_func_handle_diagonal_chains)

    def check_row_links(self):
        for r in range(2, 2 * self.rows + 1, 2):
            num_links = 0
            num_unknowns = 0
            unknown = None

            for c in range(1, 2 * self.cols + 2, 2):
                val = self.get_board(r, c)
                if val == '|':
                    num_links += 1
                elif val == ' ':
                    num_unknowns += 1
                    if num_unknowns == 1:
                        unknown = (r, c)

            if num_unknowns == 1:
                r, c = unknown
                if num_links % 2 == 0:
                    print("FOUND ROW LINK, SETTING TO X")
                    self.cond_set_x(r, c)
                else:
                    print("FOUND ROW LINK, SETTING TO |")
                    self.cond_set_link(r, c, '|')

    def check_col_links(self):
        for c in range(2, 2 * self.cols + 1, 2):
            num_links = 0
            num_unknowns = 0
            unknown = None

            for r in range(1, 2 * self.rows + 2, 2):
                val = self.get_board(r, c)
                if val == '-':
                    num_links += 1
                elif val == ' ':
                    num_unknowns += 1
                    if num_unknowns == 1:
                        unknown = (r, c)

            if num_unknowns == 1:
                r, c = unknown
                if num_links % 2 == 0:
                    self.cond_set_x(r, c)
                else:
                    self.cond_set_link(r, c, '-')

    def iter_solve(self, verbose=False):
        operations = [
            (self.handle_closed_corners, "Handling closed corners"),
            (self.fill_in_xes, "Filling in x-es based on cell values"),
            (self.fill_in_links, "Filling in links based on cell values"),
            (self.handle_threes, "Filling in links based on adjacent 3-cells"),
            (self.handle_ones, "Filling in x-es based on adjacent 1-cells"),
            (self.update_dot_state, "Filling in x-es and links based on state adjacent to dots"),
            (self.avoid_multiple_loops, "Avoid multiple loops"),
        ]

        iterator = 0
        while True:
            iterator += 1
            made_change = False

            for op in operations:
                self.set_changed(False)
                op[0]()
                if self.is_changed():
                    if verbose:
                        print("%d:  %s" % (iterator, op[1]))
                    made_change = True

            if not made_change:
                self.set_changed(False)
                self.handle_diagonal_chains()
                if self.is_changed():
                    if verbose:
                        print("%d:  (ADV) Handle diagonal chains" % iterator)
                    made_change = True

                self.set_changed(False)
                self.check_row_links()
                self.check_col_links()
                if self.is_changed():
                    if verbose:
                        print("%d:  (ADV) Check row/column links" % iterator)
                    made_change = True

            if not self.can_solve():
                print("Cannot solve this board:  invalid configuration reached.")
                break

            if not made_change:
                break

    def dots_are_connected(self, dot1, dot2):
        if dot1 in self.dot_paths and dot2 in self.dot_paths:
            dot1_path_id = self.dot_paths[dot1]
            dot2_path_id = self.dot_paths[dot2]

            return dot1_path_id == dot2_path_id
        else:
            return False

    def score_move(self, cell_r, cell_c):
        score = 0
        cell_val = self.get_board(cell_r, cell_c)
        if cell_val in ['0', '1', '2', '3']:
            cell_val = int(cell_val)
            links = self.count_adjacent_links(cell_r, cell_c)

            if cell_val == links:
                print("UNEXPECTED:  can't add another link!")

            score = 5 - cell_val + links

        return score

    def enumerate_moves(self):
        moves = set()
        for r in range(1, 2 * self.rows + 2, 2):
            for c in range(1, 2 * self.cols + 2, 2):
                if self.count_adjacent_links(r, c) == 1:
                    dot = (r, c)

                    if self.get_board(r, c - 1) == ' ' and \
                            not self.dots_are_connected(dot, (r, c - 1)):
                        score = self.score_move(r - 1, c - 1) + \
                                self.score_move(r + 1, c - 1)
                        moves.add((r, c - 1, '-', score))

                    if self.get_board(r, c + 1) == ' ' and \
                            not self.dots_are_connected(dot, (r, c + 1)):
                        score = self.score_move(r - 1, c + 1) + \
                                self.score_move(r + 1, c + 1)
                        moves.add((r, c + 1, '-', score))

                    if self.get_board(r - 1, c) == ' ' and \
                            not self.dots_are_connected(dot, (r - 1, c)):
                        score = self.score_move(r - 1, c - 1) + \
                                self.score_move(r - 1, c + 1)
                        moves.add((r - 1, c, '|', score))

                    if self.get_board(r + 1, c) == ' ' and \
                            not self.dots_are_connected(dot, (r + 1, c)):
                        score = self.score_move(r + 1, c - 1) + \
                                self.score_move(r + 1, c + 1)
                        moves.add((r + 1, c, '|', score))

        moves = list(moves)
        moves.sort(key=lambda m: m[3])

        return moves

    def apply_move(self, move):
        self.cond_set_link(move[0], move[1], move[2])


def load_puzzle(filename):
    f = open(filename)
    line = f.readline()
    dims = line.split()
    rows = int(dims[0])
    cols = int(dims[1])
    cell_values = []

    for i in range(rows):
        row_values = f.readline()
        cell_values.append(row_values[:-1])

    f.close()
    return Puzzle(rows, cols, cell_values)


def solve_puzzle(p):
    attempts = multiprocessing.Manager().list()
    board_set = multiprocessing.Manager().set()
    skipped = multiprocessing.Value('i', 0)
    event = multiprocessing.Event()

    assert p.can_solve(), "solve-puzzle was handed a board it couldn't solve!"

    info = (p, 1, None, 0, 0)
    attempts.append(info)

    processes = []
    num_processes = multiprocessing.cpu_count()

    for _ in range(num_processes):
        p = multiprocessing.Process(target=worker, args=(attempts, board_set, skipped, event))
        processes.append(p)
        p.start()

    for p in processes:
        p.join()

    if not event.is_set():
        print("Couldn't solve puzzle.")
        return False

    return True


def worker(attempts, board_set, skipped, event):
    while len(attempts) > 0:
        print(f"{len(attempts)} more board configurations to try.")
        info = attempts.pop(random.randint(0, len(attempts) - 1))
        (p, depth, move, i_move, n_moves) = info

        if depth > 1:
            print(f"DEPTH {depth}:  Move {i_move} of {n_moves}:  {str(move)}  Attempting to solve.")
        else:
            print("Attempting initial solution.")

        try:
            p.iter_solve()
        except MoveError:
            print("Encountered an invalid move.  Abandoning this path.")
            continue

        if p.is_solved():
            print(f"DEPTH {depth}:  SOLVED")
            p.pretty_print()
            event.set()
            return

        elif not p.can_solve():
            print("Couldn't solve this configuration, abandoning.")
            continue

        else:
            print(f"DEPTH {depth}:  NOT SOLVED (change-count = {p.change_count})")
            moves = p.enumerate_moves()
            print(f"From this board configuration, found {len(moves)} more moves.")
            total_moves = len(moves)
            new_moves = 0
            move_infos = []

            for i in range(total_moves):
                move = moves[i]
                p_copy = copy.deepcopy(p)
                p_copy.clear_changed_count()
                p_copy.apply_move(move)
                board_str = p_copy.get_board_as_string()

                with board_set.get_lock():
                    if board_str in board_set:
                        print(" * SKIPPING move - it's already enqueued")
                        with skipped.get_lock():
                            skipped.value += 1
                        continue

                    board_set.add(board_str)

                new_moves += 1
                info = (p_copy, depth + 1, move, i + 1, total_moves)
                move_infos.append(info)

            if len(move_infos) > 10:
                random.shuffle(move_infos)
                move_infos = move_infos[:10]
                new_moves = len(move_infos)

            with attempts.get_lock():
                attempts.extend(move_infos)
            print(f"Added {new_moves} new moves to the set of attempts.")
