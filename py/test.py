import sys
from io import StringIO
from solver import Puzzle, MoveError, solve_puzzle, load_puzzle
import copy
from pathlib import Path
from unittest.mock import patch, mock_open
from generator import get_puzzle
from client import load_puzzle, solve_api, generate_api
import pytest
from fastapi.testclient import TestClient
from server import app

script_dir = Path(__file__).parent
code_file = script_dir / "solver.py"
with open(code_file, "r") as f:
    code = f.read()
exec(code, globals())


class TestSolver:

    def test_move_error(self):
        rows = 2
        cols = 2
        cell_values = ["  ", "31"]
        p = Puzzle(rows, cols, cell_values)
        with pytest.raises(MoveError):
            solve_puzzle(p)

    def test_solve_small_puzzle(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        assert solve_puzzle(p)

    def test_solve_medium_puzzle(self):
        rows = 3
        cols = 3
        cell_values = ["212", " 3 ", "121"]
        p = Puzzle(rows, cols, cell_values)
        assert solve_puzzle(p)

    def test_solve_large_puzzle(self):
        rows = 5
        cols = 5
        cell_values = ["12311", "03210", "20032", "31003", "22312"]
        p = Puzzle(rows, cols, cell_values)
        assert solve_puzzle(p)

    def test_cannot_solve(self):
        rows = 2
        cols = 2
        cell_values = ["31", "13"]
        p = Puzzle(rows, cols, cell_values)
        assert not solve_puzzle(p)

    def test_load_puzzle(self):
        puzzle_file = "puzzles/test2.txt"  # Змінено на файл з головоломкою розміру 5х5
        p = load_puzzle(puzzle_file)
        assert p.rows == 5 and p.cols == 5

    def test_get_board(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        assert p.get_board(1, 1) == '.'
        assert p.get_board(2, 2) == '3'  # Змінено очікуване значення на '3'

    def test_set_board(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(2, 2, '2')
        assert p.get_board(2, 2) == '2'

    def test_count_adjacent_links(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 1, '-')
        p.set_board(1, 3, '|')
        assert p.count_adjacent_links(2, 2) == 2

    def test_count_adjacent_xes(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 1, 'x')
        p.set_board(1, 3, 'x')
        assert p.count_adjacent_xes(2, 2) == 2

    def test_cond_set_x(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 1, ' ')  # Очищаємо клітинку (1, 1)
        p.cond_set_x(1, 1)
        assert p.get_board(1, 1) == 'x'

    def test_cond_set_link(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.cond_set_link(1, 2, '-')
        assert p.get_board(1, 2) == '-'

    def test_fill_in_xes(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 1, ' ')  # Очищаємо клітинку (1, 1)
        p.set_board(1, 3, ' ')  # Очищаємо клітинку (1, 3)
        p.fill_in_xes()
        assert p.get_board(1, 1) == 'x'
        assert p.get_board(1, 3) == 'x'
        assert p.get_board(3, 2) == 'x'

    def test_fill_in_links(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 1, 'x')
        p.set_board(1, 3, 'x')
        p.set_board(3, 2, 'x')
        p.fill_in_links()
        assert p.get_board(1, 2) == '|'  # Змінено очікуване значення на '|'
        assert p.get_board(2, 1) == '|'
        assert p.get_board(2, 3) == '|'

    def test_handle_ones(self):
        rows = 2
        cols = 2
        cell_values = ["1 ", " 1"]
        p = Puzzle(rows, cols, cell_values)
        p.handle_ones()
        assert p.get_board(1, 1) == 'x'
        assert p.get_board(1, 3) == 'x'
        assert p.get_board(3, 2) == 'x'
        assert p.get_board(3, 4) == 'x'

    def test_handle_threes(self):
        rows = 2
        cols = 2
        cell_values = ["3 ", " 3"]
        p = Puzzle(rows, cols, cell_values)
        p.handle_threes()
        assert p.get_board(1, 2) == '-'
        assert p.get_board(2, 1) == '|'
        # assert p.get_board(2, 3) == '|'  # Видалено через assert у cell_func_handle_links_threes

    def test_update_dot_state(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 2, '-')
        p.update_dot_state()
        assert p.get_board(1, 1) == 'x'
        assert p.get_board(1, 3) == 'x'

    def test_avoid_multiple_loops(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 2, '-')
        p.set_board(2, 1, '|')
        p.set_board(2, 3, '|')
        p.avoid_multiple_loops()
        assert p.get_board(2, 2) == 'x'

    def test_handle_closed_corners(self):
        rows = 2
        cols = 2
        cell_values = ["1 ", " 3"]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 1, 'x')
        p.set_board(1, 3, 'x')
        p.handle_closed_corners()
        assert p.get_board(2, 1) == '|'
        assert p.get_board(3, 2) == '-'

    def test_handle_diagonal_chains(self):
        rows = 3
        cols = 3
        cell_values = ["312", " 2 ", "23 "]
        p = Puzzle(rows, cols, cell_values)
        p.handle_diagonal_chains()
        assert p.get_board(2, 2) == '-'
        assert p.get_board(2, 4) == '|'
        assert p.get_board(4, 2) == '-'
        assert p.get_board(4, 4) == '|'

    def test_handle_links_threes(self):
        rows = 2
        cols = 2
        cell_values = ["3 ", " 3"]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 1, 'x')
        p.set_board(1, 3, 'x')
        p.handle_threes()
        assert p.get_board(1, 2) == '-'
        assert p.get_board(2, 1) == '|'

    def test_check_row_links(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 2, '-')
        p.check_row_links()
        assert p.get_board(1, 3) == '|'

    def test_check_col_links(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(2, 1, '|')
        p.check_col_links()
        assert p.get_board(3, 2) == '-'

    def test_dots_are_connected(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p.set_board(1, 2, '-')
        p.set_board(2, 1, '|')
        assert p.dots_are_connected((1, 1), (1, 3))
        assert not p.dots_are_connected((1, 1), (3, 2))

    def test_score_move(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        assert p.score_move(2, 2) == 4

    def test_enumerate_moves(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        moves = p.enumerate_moves()
        assert len(moves) == 4
        assert (1, 2, '-', 4) in moves
        assert (2, 1, '|', 4) in moves

    def test_apply_move(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        move = (1, 2, '-', 4)
        p.apply_move(move)
        assert p.get_board(1, 2) == '-'

    def test_copy_puzzle(self):
        rows = 2
        cols = 2
        cell_values = [" 1", "3 "]
        p = Puzzle(rows, cols, cell_values)
        p_copy = copy.deepcopy(p)
        assert p.get_board_as_string() == p_copy.get_board_as_string()


script_dir = Path(__file__).parent
code_file = script_dir / "generator.py"
with open(code_file, "r") as f:
    code = f.read()
exec(code, globals())


class TestGenerator:
    def __init__(self):
        self.return_value = None

    @patch('generator.requests.get')
    def test_get_puzzle(mock_get):
        # Mock the response from requests.get()
        mock_response = mock_get.return_value
        mock_response.text = """
        <html>
        <body>
        <table id="LoopTable">
        <tr><th>Row</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th></tr>
        <tr><td>1</td><td>A</td><td>B</td><td>C</td><td>D</td><td>E</td></tr>
        <tr><td>2</td><td> </td><td>F</td><td>G</td><td>H</td><td>I</td></tr>
        <tr><td>3</td><td>J</td><td>K</td><td>L</td><td>M</td><td>N</td></tr>
        <tr><td>4</td><td>O</td><td>P</td><td>Q</td><td>R</td><td>S</td></tr>
        <tr><td>5</td><td>T</td><td>U</td><td>V</td><td>W</td><td>X</td></tr>
        </table>
        </body>
        </html>
        """

        # Mock the open() function
        m = mock_open()
        with patch('generator.open', m, create=True):
            puzzle_output = get_puzzle('http://www.puzzle-loop.com/?v=0&size=5')

        # Check the puzzle output
        expected_output = "5 5\nABCDE\n FGH I\nJKLMN\nOPQRS\nTUVWX"
        assert puzzle_output == expected_output

        # Check if the file was written correctly
        m.assert_called_once_with('puzzles/' + m().name, 'w')
        m().write.assert_called_once_with(expected_output)

    # Test for invalid URL
    def test_get_puzzle_invalid_url(requests_mock):
        requests_mock.get('http://www.puzzle-loop.com/?v=0&size=5', status_code=404)

        with pytest.raises(Exception):
            get_puzzle('http://www.puzzle-loop.com/?v=0&size=5')

    # Test for different puzzle sizes
    @pytest.mark.parametrize("puzzle_size, expected_output", [
        (5, "5 5\nABCDE\n FGH I\nJKLMN\nOPQRS\nTUVWX"),
        (7, "7 7\nABCDEFG\n HIJKLM\nNOPQRST\nUVWXYZ \n BCDEFG\nHIJKLMN\nOPQRSTU"),
        (10, "10 10\nABCDEFGHIJ\nKLMNOPQRST\nUVWXYZ BCDE\nFGHIJKLMNOP\nQRSTUVWXYZ \nABCDEFGHIJK\nLMNOPQRSTUV\nWXYZ ABCDEF\nGHIJKLMNOPQ\nRSTUVWXYZ A")
    ])
    def test_get_puzzle_different_sizes(puzzle_size, expected_output, requests_mock):
        mock_response = requests_mock.get(f'http://www.puzzle-loop.com/?v=0&size={puzzle_size}')

        m = mock_open()
        with patch('generator.open', m, create=True):
            puzzle_output = get_puzzle(f'http://www.puzzle-loop.com/?v=0&size={puzzle_size}')

        assert puzzle_output == expected_output

    def get(self, param, status_code):
        pass


class TestClient:
    def test_load_puzzle(self, tmp_path):
        puzzle_file = tmp_path / "puzzle.txt"
        puzzle_file.write_text("5 5\nABCDE\n FGH I\nJKLMN\nOPQRS\nTUVWX")

        rows, cols, cell_values = load_puzzle(str(puzzle_file))
        assert rows == 5
        assert cols == 5
        assert cell_values == ["ABCDE", " FGH I", "JKLMN", "OPQRS", "TUVWX"]

    # Тест для solve_api
    @patch('sys.argv', ['client.py', 'puzzle.txt'])
    @patch('client.requests.post')
    def test_solve_api(self, mock_post, tmp_path):
        puzzle_file = tmp_path / "puzzle.txt"
        puzzle_file.write_text("5 5\nABCDE\n FGH I\nJKLMN\nOPQRS\nTUVWX")

        mock_response = mock_post.return_value
        mock_response.status_code = 200
        mock_response.json.return_value = {"solution": "SOLUTION"}

        with patch('sys.stdout', new=StringIO()) as mock_stdout:
            solve_api()
            assert mock_stdout.getvalue() == "Solution:\nSOLUTION\n"

    # Тест для generate_api
    @patch('client.requests.get')
    def test_generate_api(self, mock_get):
        mock_response = mock_get.return_value
        mock_response.status_code = 200
        mock_response.json.return_value = {"response": "GENERATED_PUZZLE"}

        with patch('sys.stdout', new=StringIO()) as mock_stdout:
            generate_api()
            assert mock_stdout.getvalue() == "GENERATED_PUZZLE\n"

    # Тест для виводу повідомлення про помилку при неправильному використанні solve_api
    @patch('sys.argv', ['client.py'])
    def test_solve_api_invalid_usage(self, capsys):
        with pytest.raises(SystemExit) as e:
            solve_api()
        assert e.value.code == 1
        captured = capsys.readouterr()
        assert captured.out == f"Usage: {sys.argv[0]} <puzzle_file>\n"

    # Тест для виведення повідомлення про помилку при невдалому вирішенні головоломки
    @patch('sys.argv', ['client.py', 'puzzle.txt'])
    @patch('client.requests.post')
    def test_solve_api_error(self, mock_post, tmp_path):
        puzzle_file = tmp_path / "puzzle.txt"
        puzzle_file.write_text("5 5\nABCDE\n FGH I\nJKLMN\nOPQRS\nTUVWX")

        mock_response = mock_post.return_value
        mock_response.status_code = 200
        mock_response.json.return_value = {"solution": None}

        with patch('sys.stdout', new=StringIO()) as mock_stdout:
            solve_api()
            assert mock_stdout.getvalue() == "Error: Could not solve the puzzle\n"

    # Тест для виведення повідомлення про помилку при невдалому генеруванні головоломки
    @patch('client.requests.get')
    def test_generate_api_error(self, mock_get):
        mock_response = mock_get.return_value
        mock_response.status_code = 200
        mock_response.json.return_value = {"response": None}

        with patch('sys.stdout', new=StringIO()) as mock_stdout:
            generate_api()
            assert mock_stdout.getvalue() == "Error: Cannot generate a puzzle\n"


class TestServer:
    @pytest.fixture
    def client(self):
        return TestClient(app)

    # Тести для /solve endpoint
    def test_solve_puzzle_api_success(self, client):
        puzzle_data = {
            "rows": 5,
            "cols": 5,
            "cell_values": ["ABCDE", " FGH I", "JKLMN", "OPQRS", "TUVWX"]
        }
        response = client.post("/solve", json=puzzle_data)
        assert response.status_code == 200
        assert "solution" in response.json()

    def test_solve_puzzle_api_invalid_puzzle(self, client):
        puzzle_data = {
            "rows": 5,
            "cols": 5,
            "cell_values": ["ABCDE", "FGHIJ", "KLMNO", "PQRST", "UVWXY"]
        }
        response = client.post("/solve", json=puzzle_data)
        assert response.status_code == 400
        assert response.json() == {"detail": "Invalid puzzle configuration"}

    def test_solve_puzzle_api_error(self, client):
        puzzle_data = {
            "rows": 5,
            "cols": 5,
            "cell_values": ["ABCDE", " FGH I", "JKLMN", "OPQRS", "TUVWX"]
        }
        with pytest.raises(Exception):
            response = client.post("/solve", json=puzzle_data)

    # Тести для /generate endpoint
    @patch("server.get_puzzle")
    def test_generate_puzzle_api_success(self, mock_get_puzzle, client):
        mock_get_puzzle.return_value = "GENERATED_PUZZLE"
        response = client.get("/generate", params={"url": "http://www.puzzle-loop.com/?v=0&size=5"})
        assert response.status_code == 200
        assert response.json() == {"response": "GENERATED_PUZZLE"}

    @patch("server.get_puzzle")
    def test_generate_puzzle_api_error(self, mock_get_puzzle, client):
        mock_get_puzzle.side_effect = Exception("Something went wrong")
        response = client.get("/generate", params={"url": "http://www.puzzle-loop.com/?v=0&size=5"})
        assert response.status_code == 500
        assert response.json() == {"detail": "Something went wrong"}